const knex = require("../../connection.js");
const moment = require("moment");
const sha = require("sha256");

function getDateTime() {
  return moment().format("YYYY-MM-DD HH:mm:ss");
}

function dateFormat(date) {
  return moment(date, "DD-MM-YYYY").format("YYYY-MM-DD");
}

async function insertUser(
  user_id,
  user_name,
  user_password,
  user_email,
  user_tel,
  user_age,
  user_dob,
  user_status,
  user_address
) {
  console.log(`Passed`)
  let result = null;

  try {
    

    let sql = await knex.connect(`testing`).insert({
      user_id:user_id,
      user_name:user_name,
      user_password: user_password,
      user_email: user_email,
      user_tel:user_tel,
      user_age:user_age,
      user_address:user_address,
      user_dob:user_dob,
      user_status:"Active",
    });

    console.log("insertEjen: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }

  } catch (error) {
    console.log(error);
  }

  return result;
}
async function updateUser(
  user_id,
  user_name,
  user_password,
  user_email,
  user_tel,
  user_age,
  user_dob,
  user_status,
  user_address
) {
  
  
  let result = null;

  try {

    

      let sql = await knex.connect(`testing`).update({
      
      user_name:user_name,
      user_password: user_password,
      user_email: user_email,
      user_tel:user_tel,
      user_age:user_age,
      user_address:user_address,
      user_dob:user_dob,
      user_status:user_status,
      })
      .where(`user_id`, user_id);
      console.log("updateEjen: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }

  } catch (error) {
    console.log(error);
  }

  return result;

}

module.exports = {
  insertUser,
  updateUser,

};