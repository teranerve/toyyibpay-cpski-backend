const knex = require("../../connection.js");
const moment = require("moment");

function check_param(body) {
  if (!body.stok || body.stok.length < 1)
    return { status: false, message: "Stok is required" };

  if (!body.status) return { status: false, message: "Status is required" };

  if (body.status != "Active" && body.status != "Inactive")
    return { status: false, message: "Status must be Active or Inactive" };

  return { status: true };
}

async function make_stok_inactive(body) {
  // GET INDEX 0 FROM body.stok
  const stok = body.stok.length;

  // LOOP EVERY STOK
  for (let i = 0; i < body.stok.length; i++) {
    var implementorLokasiHaiwanId = await knex
      .connect("liveStok")
      .select("implementerLokasiHaiwanId as id")
      .where("liveStokNo", body.stok[i])
      .first();

    if (!implementorLokasiHaiwanId) return false;

    var jumlah_bahagian = await knex
      .connect("liveStokBahagian")
      .select("liveStokBahagianNo as BAHAGIAN_NO")
      .whereLike("liveStokBahagianNo", `%${body.stok[i]}%`);
    console.log("bhgn: ", jumlah_bahagian.length);

    // UPDATE STOK STATUS
    var update = await knex
      .connect("liveStok")
      .update({
        liveStokStatus: body.status,
      })
      .whereLike("liveStokNo", body.stok[i]);
    console.log("update livestok status: ", update);

    // UPDATE LIST BAHAGIAN STATUS
    var update1 = await knex
      .connect("liveStokBahagian")
      .update({
        liveStokBahagianStatus: body.status,
      })
      .whereLike("liveStokBahagianNo", `%${body.stok[i]}%`);

    // UPDATE IMPLEMENETOR LOKASI HAIWAN

    if (body.status == "Active") {
      var updateImplementorLokasiHaiwan = await knex
        .connect("implementerLokasiHaiwan")
        .update({
          jumlahKeseluruhan: knex.connect.raw(`jumlahKeseluruhan + 1`),
          jumlahBaki: knex.connect.raw(`jumlahBaki + 1`),
          jumlahKeseluruhanBhgn: knex.connect.raw(
            `jumlahKeseluruhanBhgn + ${jumlah_bahagian.length}`
          ),
          jumlahBakiBhgn: knex.connect.raw(
            `jumlahBakiBhgn + ${jumlah_bahagian.length}`
          ),
        })
        .where("implementerLokasiHaiwanId", implementorLokasiHaiwanId.id);
    } else if (body.status == "Inactive") {
      var updateImplementorLokasiHaiwan = await knex
        .connect("implementerLokasiHaiwan")
        .update({
          jumlahKeseluruhan: knex.connect.raw(`jumlahKeseluruhan - 1`),
          jumlahBaki: knex.connect.raw(`jumlahBaki - 1`),
          jumlahKeseluruhanBhgn: knex.connect.raw(
            `jumlahKeseluruhanBhgn - ${jumlah_bahagian.length}`
          ),
          jumlahBakiBhgn: knex.connect.raw(
            `jumlahBakiBhgn - ${jumlah_bahagian.length}`
          ),
        })
        .where("implementerLokasiHaiwanId", implementorLokasiHaiwanId.id);
    }
  }

  return true;
}

module.exports = {
  check_param,
  make_stok_inactive,
};
