const knex = require("../../connection.js");
const moment = require("moment");

function getDateTime() {
  return moment().format("YYYY-MM-DD HH:mm:ss");
}

function dateFormat(date) {
  return moment(date, "DD-MM-YYYY").format("YYYY-MM-DD");
}

async function insertLiveStok(
  implementerId,
  implementerLokasiId,
  implementerLokasiHaiwanId,
  liveStokNo,
  liveStokType,
  liveStokTypeCode,
  liveStokBilBahagian,
  jenisIbadah,
  setupKorbanId
) {
  let result = null;

  try {
    let datetime = getDateTime();

    let sql = await knex.connect(`liveStok`).insert({
      implementerId: implementerId,
      implementerLokasiId: implementerLokasiId,
      implementerLokasiHaiwanId: implementerLokasiHaiwanId,
      setupKorbanId: "1", //hardcode
      organizationId: "1", //hardcode
      liveStokNo: liveStokNo,
      liveStokType: liveStokType,
      liveStokTypeCode: liveStokTypeCode,
      liveStokIbadah: jenisIbadah,
      liveStokBilBahagian: liveStokBilBahagian,
      liveStokBilBahagianBaki: liveStokBilBahagian,
      liveStokStatus: "Active",
      liveStokStatusCode: "20",
      setupKorbanId: setupKorbanId,
    });

    console.log("insertLiveStok: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function insertLiveStokBahagian(
  latestLiveStokId,
  liveStokBahagianNo,
  setupKorbanId
) {
  let result = null;

  try {
    let sql = await knex.connect(`liveStokBahagian`).insert({
      liveStokId: latestLiveStokId,
      liveStokBahagianNo: liveStokBahagianNo,
      liveStokBahagianStatus: "Active",
      liveStokBahagianStatusCode: "20",
      setupKorbanId: setupKorbanId,
    });

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function updateImplementerLokasiHaiwan(
  implementerLokasiHaiwanId,
  stokBaru,
  jumlahBahagian
) {
  let result = null;

  try {
    let sql = await knex
      .connect(`implementerLokasiHaiwan`)
      .where("implementerLokasiHaiwanId", implementerLokasiHaiwanId)
      .update({
        jumlahKeseluruhan: knex.connect.raw(`jumlahKeseluruhan + ${stokBaru}`),
        jumlahBaki: knex.connect.raw(`jumlahBaki + ${stokBaru}`),
        jumlahKeseluruhanBhgn: knex.connect.raw(
          `jumlahKeseluruhanBhgn + ${jumlahBahagian}`
        ),
        jumlahBakiBhgn: knex.connect.raw(`jumlahBakiBhgn + ${jumlahBahagian}`),
      });

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}
async function updateLiveStoktoInActive(liveStokId) {
  let result = null;

  try {
    let sql = await knex
      .connect(`liveStok`)
      .update({ liveStokStatus: "InActive" })
      .where(`liveStokId`, liveStokId);
    console.log("LiveStok Id: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}
async function deleteLiveStok(liveStokId) {
  let result = null;

  try {
    let sql = await knex
      .connect(`liveStok`)
      .update({ liveStokStatus: "Deleted" })
      .where(`liveStokId`, liveStokId);
    console.log("LiveStok Id: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}
async function updateLiveStoktoActive(liveStokId) {
  let result = null;

  try {
    let sql = await knex
      .connect(`liveStok`)

      .update({ liveStokStatus: "Active" })
      .where(`liveStokId`, liveStokId);
    console.log("LiveStok Id: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}
async function getImplementerDetails(implementerId) {
  let result = null;

  result = await knex
    .connect(`implementer`)
    .where(`implementerId`, implementerId);

  return result;
}

async function getLatestLiveStokId() {
  let result = null;

  result = await knex.connect.raw(
    `Select max(liveStokId) as latestId from liveStok`
  );

  return result;
}

async function getCode(type) {
  let result = null;

  result = await knex.connect
    .raw(`Select cf.configurationName CATEGORY, cl.confLookupCode TYPE,
    cf.configurationValue2 SHORTNAMES from configuration cf join
    confLookup cl
    On cl.confLookupId = cf.configurationValue1
    where cf.configurationParentId = 87`);

  return result[0];
}

async function getBilBahagian() {
  let result = null;
  result = await knex.connect
    .raw(`Select cl.confLookupCode HAIWAN, cf.configurationValue2 BAHAGIAN
    from configuration cf join confLookup cl
    On cl.confLookupId = cf.configurationValue1
    where cf.configurationParentId = 83`);

  return result;
}

async function getLiveStok(implementerLokasiHaiwanId) {
  let result = await knex
    .connect("liveStok")
    .where("implementerLokasiHaiwanId", implementerLokasiHaiwanId)
    .whereNot("liveStokStatus", "Deleted");

  let implementerLokasiHaiwan = await knex
    .connect("implementerLokasiHaiwan")
    .where("implementerLokasiHaiwanId", implementerLokasiHaiwanId);

  let implementer = await knex
    .connect("implementer")
    .where("implementerId", implementerLokasiHaiwan[0].implementerId);

  console.log(
    "------------------++++++++implementerLokasiHaiwan: ",
    implementerLokasiHaiwan
  );
  console.log("------------------++++++++implementer: ", implementer);

  return [result, implementerLokasiHaiwan, implementer];
}

async function getLiveStokInActive(implementerLokasiHaiwanId, status) {
  let result = await knex
    .connect("liveStok")
    .where("implementerLokasiHaiwanId", implementerLokasiHaiwanId)
    .where("liveStokStatus", "InActive");

  let implementerLokasiHaiwan = await knex
    .connect("implementerLokasiHaiwan")
    .where("implementerLokasiHaiwanId", implementerLokasiHaiwanId);

  let implementer = await knex
    .connect("implementer")
    .where("implementerId", implementerLokasiHaiwan[0].implementerId);

  console.log(
    "------------------++++++++implementerLokasiHaiwanInActive: ",
    implementerLokasiHaiwan
  );
  console.log("------------------++++++++implementer: ", implementer);

  return [result, implementerLokasiHaiwan, implementer];
}

async function getLiveStokActive(implementerLokasiHaiwanId) {
  let result = await knex
    .connect("liveStok")
    .where("implementerLokasiHaiwanId", implementerLokasiHaiwanId)
    .where(`liveStokStatus`, "Active")
    .where(`liveStokBilBahagianBaki`, 7);

  let implementerLokasiHaiwan = await knex
    .connect("implementerLokasiHaiwan")
    .where("implementerLokasiHaiwanId", implementerLokasiHaiwanId);

  let implementer = await knex
    .connect("implementer")
    .where("implementerId", implementerLokasiHaiwan[0].implementerId);

  console.log(
    "------------------++++++++implementerLokasiHaiwan: ",
    implementerLokasiHaiwan
  );
  console.log("------------------++++++++implementer: ", implementer);

  return [result, implementerLokasiHaiwan, implementer];
}

async function getLiveStokByStatus(implementerLokasiHaiwanId, status) {
  let result = await knex
    .connect("liveStok")
    .select("*", `liveStokStatus`)
    .where("implementerLokasiHaiwanId", implementerLokasiHaiwanId)
    .where(`liveStokStatus`, status);

  let implementerLokasiHaiwan = await knex
    .connect("implementerLokasiHaiwan")
    .where("implementerLokasiHaiwanId", implementerLokasiHaiwanId);

  let implementer = await knex
    .connect("implementer")
    .where("implementerId", implementerLokasiHaiwan[0].implementerId);

  console.log(
    "------------------++++++++implementerLokasiHaiwan: ",
    implementerLokasiHaiwan
  );
  console.log("------------------++++++++implementer: ", implementer);

  return [result, implementerLokasiHaiwan, implementer];
}

async function getLiveStokBahagian(liveStokId) {
  let result = await knex
    .connect("liveStokBahagian")
    .where("liveStokId", liveStokId);

  return result;
}

module.exports = {
  insertLiveStok,
  insertLiveStokBahagian,
  updateImplementerLokasiHaiwan,
  getImplementerDetails,
  getLatestLiveStokId,
  getCode,
  getBilBahagian,
  getLiveStok,
  getLiveStokActive,
  getLiveStokInActive,
  getLiveStokByStatus,
  getLiveStokBahagian,
  updateLiveStoktoInActive,
  updateLiveStoktoActive,
  deleteLiveStok,
};
