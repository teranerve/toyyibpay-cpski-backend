const sha = require("sha256");
const knex = require("../../connection.js");
const moment = require("moment");

function getDateTime() {
  return moment().format("YYYY-MM-DD HH:mm:ss");
}

function dateFormat(date) {
  return moment(date, "DD-MM-YYYY").format("YYYY-MM-DD");
}

async function getCurrentSetupKorbanId() {
  let year = moment().format("YYYY");

  let setupKorban = await knex
    .connect("setupKorban")
    .where("setupKorbanTahun", year)
    .first();

  if (!setupKorban) return false;

  return setupKorban.setupKorbanId;
}

//INSERT
async function insertImplementer(
  username,
  fullname,
  email,
  password,
  phoneno,
  negeri,
  negara,
  ibadah,
  setupKorbanId
) {
  let result = null;
  let kawasan = null;
  let implementerStatus = null;
  let implementerStatusCode = null;

  try {
    let datetime = getDateTime();

    if (
      negara == "Semenanjung" ||
      negara == "Sabah" ||
      negara == "Sarawak" ||
      negara == "Orang Asli" ||
      negara == "Semenanjung & Sabah"
    ) {
      kawasan = "Malaysia";
    } else {
      kawasan = "Luar Negara";
    }

    if (negara == "Chad" || negara == "Nigeria") {
      implementerStatus = "Inactive";
      implementerStatusCode = 21;
    } else {
      implementerStatus = "Active";
      implementerStatusCode = 20;
    }

    result = await knex.connect(`implementer`).insert({
      implementerUsername: username,
      implementerFullname: fullname,
      implementerEmail: email,
      implementerPassword: sha(password),
      implementerPhoneNo: phoneno,
      implementerRegisterDate: datetime,
      implementerStatus: implementerStatus,
      implementerStatusCode: implementerStatusCode,
      implementerKawasan: kawasan,
      implementerNegara: negara,
      implementerIbadah: ibadah,
      setupKorbanId: setupKorbanId,
      // implementerLokasiHaiwanJenis:haiwan,
    });

    console.log(result);
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function insertImplementerLokasi(
  insertImplementer,
  alamat,
  postcode,
  bandar,
  negeri,
  noakaun,
  namapemegangbank,
  namabank,
  bankcode,
  closedate,
  setupKorbanId
) {
  let result = null;

  try {
    let datetime = getDateTime();

    result = await knex.connect(`implementerLokasi`).insert({
      implementerId: insertImplementer,
      implementerLokasiAlamat: alamat,
      implementerLokasiPostcode: postcode,
      implementerLokasiBandar: bandar,
      implementerLokasiNegeri: negeri,
      implementerLokasiOrderCloseDate: closedate,
      implementerLokasiStatus: "Active",
      implementerLokasiBankAccNo: noakaun,
      implementerLokasiBankHolderName: namapemegangbank,
      implementerLokasiBankName: namabank,
      implementerLokasiBankCode: bankcode,
      setupKorbanId: setupKorbanId,
    });

    console.log(result);
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function insertImplementerLokasiHaiwan(
  insertImplementer,
  insertImplementerLokasi,
  jenis,
  hargaekor,
  hargabhg,
  statusHaiwanKorban,
  setupKorbanId
) {
  let result = null;

  try {
    let datetime = getDateTime();

    var lembu = null;
    var kambing = null;
    var unta = null;

    result = [
      {
        implementerId: insertImplementer,
        implementerLokasiId: insertImplementerLokasi,
        implementerLokasiHaiwanJenis: jenis,
        implementerLokasiHaiwanHargaEkor: hargaekor,
        implementerLokasiHaiwanHargaBhgn: hargabhg,
        implementerLokasiHaiwanStatus: statusHaiwanKorban,
        implementerLokasiHaiwanStatusCode: 20,
        jumlahKeseluruhan: 0,
        jumlahBaki: 0,
        jumlahKeseluruhanBhgn: 0,
        jumlahBakiBhgn: 0,
      },
    ];

    console.log(jenis);

    for (let i = 0; i < jenis.length; i++) {
      if (statusHaiwanKorban[i] == "Active") {
        let [user1] = await knex.connect("implementerLokasiHaiwan").insert({
          implementerId: insertImplementer,
          implementerLokasiId: insertImplementerLokasi,
          implementerLokasiHaiwanJenis: jenis[i],
          implementerLokasiHaiwanHargaEkor: hargaekor[i],
          implementerLokasiHaiwanHargaBhgn: hargabhg[i],
          implementerLokasiHaiwanStatus: statusHaiwanKorban[i],
          implementerLokasiHaiwanStatusCode: 20,
          jumlahKeseluruhan: 0,
          jumlahBaki: 0,
          jumlahKeseluruhanBhgn: 0,
          jumlahBakiBhgn: 0,
          setupKorbanId: setupKorbanId,
        });
      } else {
        let [user1] = await knex.connect("implementerLokasiHaiwan").insert({
          implementerId: insertImplementer,
          implementerLokasiId: insertImplementerLokasi,
          implementerLokasiHaiwanJenis: jenis[i],
          implementerLokasiHaiwanHargaEkor: hargaekor[i],
          implementerLokasiHaiwanHargaBhgn: hargabhg[i],
          implementerLokasiHaiwanStatus: statusHaiwanKorban[i],
          implementerLokasiHaiwanStatusCode: 21,
          jumlahKeseluruhan: 0,
          jumlahBaki: 0,
          jumlahKeseluruhanBhgn: 0,
          jumlahBakiBhgn: 0,
          setupKorbanId: setupKorbanId,
        });
      }
    }
    status = "berjaya";
    message = "masuk dah dalam db";
  } catch (error) {
    status = "unsuccess";
    message = "API error. " + error;
  }

  return result;
}

async function insertsetupKorbanImplementerLokasi(
  insertImplementer,
  insertImplementerLokasi,
  setupKorbanId
) {
  let result = null;

  try {
    let datetime = getDateTime();

    result = await knex.connect(`setupKorbanImplementerLokasi`).insert({
      implementerId: insertImplementer,
      implementerLokasiId: insertImplementerLokasi,
      setupKorbanId: setupKorbanId,
    });

    console.log(result);
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function displayImplementer() {
  let result = null;
  let count = null;

  // dateFrom = dateFormat(dateFrom);
  // console.log(`dateFrom: `, dateFrom);

  // dateTo = dateFormat(dateTo);
  // console.log(`dateTo: `, dateTo);

  result = await knex.connect(`implementer`);
  count = await knex.connect(`implementer`).count();

  console.log(result);

  return [result, count[0]["count(*)"]];
}

async function getImplementerButiran(implementerId) {
  let result = null;

  // dateFrom = dateFormat(dateFrom);
  // console.log(`dateFrom: `, dateFrom);

  // dateTo = dateFormat(dateTo);
  // console.log(`dateTo: `, dateTo);

  result = await knex
    .connect(`implementer AS i`)
    .leftJoin("implementerLokasi AS il", "il.implementerId", "i.implementerId")
    .leftJoin(
      "implementerLokasiHaiwan AS ilh",
      "ilh.implementerId",
      "i.implementerId"
    )
    .leftJoin(
      "setupKorbanImplementerLokasi AS skil",
      "skil.implementerId",
      "i.implementerId"
    )
    .where("i.implementerId", implementerId)
    .orderBy("ilh.implementerLokasiHaiwanJenis", "desc");
  // .where(`userStatusCode`, userStatusCode)
  // .where(`userStatusCode`, userStatusCode)

  // console.log(count[0]["count(*)"]);

  // console.log("result: ", result);

  return result;
}

async function checkUsernameEmail(username, email, phoneno) {
  let result = null;

  try {
    let sql = await knex
      .connect(`implementer`)
      .where(`implementerUsername`, username)
      .orWhere(`implementerEmail`, email)
      .orWhere("implementerPhoneNo", phoneno);

    console.log("getUser: ", sql);
    if (!sql || sql.length == 0) {
      result = true;
    } else {
      result = false;
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function checkUsername(implementerId, username) {
  let result = null;

  try {
    let sql = await knex
      .connect(`implementer`)
      .where(`implementerUsername`, username)
      .where("implementerId", "!=", implementerId);

    console.log("getUser: ", implementerId);
    if (!sql || sql.length == 0) {
      result = true;
    } else {
      result = false;
    }

    // if(sql[0]['userUsername'] == userUsername ){
    //   if (sql[0]['userEmail'] == userEmail){
    //     if(sql[0]['userPhoneNo'] == userPhoneNo){
    //     result = false;
    //     }
    //   }

    // }else{
    //   if (sql[0]['userEmail'] == userEmail){
    //     if (sql[0]['userPhoneNo'] == userPhoneNo){

    //     }

    //   }
    // }

    // if(sql[0]['userUsername'] == userUsername && sql[0]['userEmail'] == userEmail && sql[0]['userPhoneNo'] == userPhoneNo){
    //   result = false;
    // }else if(sql[0]['userUsername'] == userUsername || sql[0]['userEmail'] == userEmail || sql[0]['userPhoneNo'] == userPhoneNo){
    //   result = true;
    // }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function checkEmail(implementerId, email) {
  let result = null;

  try {
    let sql = await knex
      .connect(`implementer`)
      .where(`implementerEmail`, email)
      .where("implementerId", "!=", implementerId);

    console.log("getUser: ", implementerId);
    if (!sql || sql.length == 0) {
      result = true;
    } else {
      result = false;
    }

    // if(sql[0]['userUsername'] == userUsername ){
    //   if (sql[0]['userEmail'] == userEmail){
    //     if(sql[0]['userPhoneNo'] == userPhoneNo){
    //     result = false;
    //     }
    //   }

    // }else{
    //   if (sql[0]['userEmail'] == userEmail){
    //     if (sql[0]['userPhoneNo'] == userPhoneNo){

    //     }

    //   }
    // }

    // if(sql[0]['userUsername'] == userUsername && sql[0]['userEmail'] == userEmail && sql[0]['userPhoneNo'] == userPhoneNo){
    //   result = false;
    // }else if(sql[0]['userUsername'] == userUsername || sql[0]['userEmail'] == userEmail || sql[0]['userPhoneNo'] == userPhoneNo){
    //   result = true;
    // }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function checkPhone(implementerId, phoneno) {
  let result = null;

  try {
    let sql = await knex
      .connect(`implementer`)
      .where(`implementerPhoneNo`, phoneno)
      .where("implementerId", "!=", implementerId);

    console.log("getUser: ", implementerId);
    if (!sql || sql.length == 0) {
      result = true;
    } else {
      result = false;
    }

    // if(sql[0]['userUsername'] == userUsername ){
    //   if (sql[0]['userEmail'] == userEmail){
    //     if(sql[0]['userPhoneNo'] == userPhoneNo){
    //     result = false;
    //     }
    //   }

    // }else{
    //   if (sql[0]['userEmail'] == userEmail){
    //     if (sql[0]['userPhoneNo'] == userPhoneNo){

    //     }

    //   }
    // }

    // if(sql[0]['userUsername'] == userUsername && sql[0]['userEmail'] == userEmail && sql[0]['userPhoneNo'] == userPhoneNo){
    //   result = false;
    // }else if(sql[0]['userUsername'] == userUsername || sql[0]['userEmail'] == userEmail || sql[0]['userPhoneNo'] == userPhoneNo){
    //   result = true;
    // }
  } catch (error) {
    console.log(error);
  }

  return result;
}

//UPDATE
async function updateImplementer(
  implementerId,
  username,
  fullname,
  email,
  password,
  phoneno,
  negeri,
  negara,
  ibadah,
  implementerStatus
) {
  let result = null;
  let kawasan = null;

  try {
    let datetime = getDateTime();
    let sql2 = await knex
      .connect(`implementer`)
      .select(`implementerId`, `implementerPassword`)
      .where(`implementerId`, implementerId);

    if (
      negara == "Semenanjung" ||
      negara == "Sabah" ||
      negara == "Sarawak" ||
      negara == "Orang Asli"
    ) {
      kawasan = "Malaysia";
    } else {
      kawasan = "Luar Negara";
    }
    if (sql2[0].implementerPassword == password) {
      if (implementerStatus == "Active") {
        result = await knex
          .connect(`implementer`)
          .update({
            implementerId: implementerId,
            implementerUsername: username,
            implementerFullname: fullname,
            implementerEmail: email,
            implementerPassword: password,
            implementerPhoneNo: phoneno,
            implementerStatus: implementerStatus,
            implementerStatusCode: 20,
            implementerKawasan: kawasan,
            implementerNegara: negara,
            implementerIbadah: ibadah,
          })
          .where(`implementerId`, implementerId);
      } else {
        result = await knex
          .connect(`implementer`)
          .update({
            implementerId: implementerId,
            implementerUsername: username,
            implementerFullname: fullname,
            implementerEmail: email,
            implementerPassword: password,
            implementerPhoneNo: phoneno,
            implementerStatus: implementerStatus,
            implementerStatusCode: 21,
            implementerKawasan: kawasan,
            implementerNegara: negara,
            implementerIbadah: ibadah,
          })
          .where(`implementerId`, implementerId);
      }

      console.log(result);
    } else {
      if (implementerStatus) {
        result = await knex
          .connect(`implementer`)
          .update({
            implementerId: implementerId,
            implementerUsername: username,
            implementerFullname: fullname,
            implementerEmail: email,
            implementerPassword: sha(password),
            implementerPhoneNo: phoneno,
            implementerStatus: implementerStatus,
            implementerStatusCode: 20,
            implementerKawasan: negeri,
            implementerNegara: negara,
            implementerIbadah: ibadah,
          })
          .where(`implementerId`, implementerId);
      } else {
        result = await knex
          .connect(`implementer`)
          .update({
            implementerId: implementerId,
            implementerUsername: username,
            implementerFullname: fullname,
            implementerEmail: email,
            implementerPassword: sha(password),
            implementerPhoneNo: phoneno,
            implementerStatus: implementerStatus,
            implementerStatusCode: 21,
            implementerKawasan: negeri,
            implementerNegara: negara,
            implementerIbadah: ibadah,
          })
          .where(`implementerId`, implementerId);
      }

      console.log(result);
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function updateImplementerLokasi(
  implementerId,
  implementerLokasiId,
  alamat,
  postcode,
  bandar,
  negeri,
  noakaun,
  namapemegangbank,
  namabank,
  bankcode,
  closedate
) {
  let result = null;

  try {
    result = await knex
      .connect(`implementerLokasi`)
      .update({
        implementerId: implementerId,
        implementerLokasiAlamat: alamat,
        implementerLokasiPostcode: postcode,
        implementerLokasiBandar: bandar,
        implementerLokasiNegeri: negeri,
        implementerLokasiOrderCloseDate: closedate,
        implementerLokasiStatus: "Active",
        implementerLokasiBankAccNo: noakaun,
        implementerLokasiBankHolderName: namapemegangbank,
        implementerLokasiBankName: namabank,
        implementerLokasiBankCode: bankcode,
      })
      .where(`implementerId`, implementerId);

    console.log(result);
    console.log("Dah lalu lokasi");
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function updateImplementerLokasiHaiwan(
  implementerId,
  implementerLokasiId,
  jenis,
  hargaekor,
  hargabhg,
  statusHaiwanKorban
) {
  let result = null;

  let implementerLokasiHaiwanId = [];
  result = await knex
    .connect("implementerLokasiHaiwan")
    .where({ implementerId: implementerId })
    .select("implementerLokasiHaiwanId");
  implementerLokasiHaiwanId = result;

  try {
    //   result = ([{
    //     implementerId:implementerId,
    //     implementerLokasiId:7,
    //     implementerLokasiHaiwanJenis: jenis,
    //     implementerLokasiHaiwanHargaEkor: hargaekor,
    //     implementerLokasiHaiwanHargaBhgn: hargabhg,
    //     implementerLokasiHaiwanStatus: statusHaiwanKorban,
    //     implementerLokasiHaiwanStatusCode: 20,
    //     jumlahKeseluruhan:0,
    //     jumlahBaki:0,
    //     jumlahKeseluruhanBhgn:0,
    //     jumlahBakiBhgn:0
    // }]);
    console.log("Dah lalu sini");
    for (let i = 0; i < jenis.length; i++) {
      if (statusHaiwanKorban[i] == "Active") {
        let result = null;
        result = await knex
          .connect("implementerLokasiHaiwan")
          .where({
            implementerLokasiHaiwanId:
              implementerLokasiHaiwanId[i].implementerLokasiHaiwanId,
          })
          .update({
            implementerLokasiHaiwanJenis: jenis[i],
            implementerLokasiHaiwanHargaEkor: hargaekor[i],
            implementerLokasiHaiwanHargaBhgn: hargabhg[i],
            implementerLokasiHaiwanStatus: statusHaiwanKorban[i],
            implementerLokasiHaiwanStatusCode: 20,
          });
      } else {
        console.log(jenis[i]);
        console.log(implementerLokasiHaiwanId[i].implementerLokasiHaiwanId);
        let result = null;
        result = await knex
          .connect("implementerLokasiHaiwan")
          .where({
            implementerLokasiHaiwanId:
              implementerLokasiHaiwanId[i].implementerLokasiHaiwanId,
          })
          .update({
            implementerLokasiHaiwanJenis: jenis[i],
            implementerLokasiHaiwanHargaEkor: hargaekor[i],
            implementerLokasiHaiwanHargaBhgn: hargabhg[i],
            implementerLokasiHaiwanStatus: statusHaiwanKorban[i],
            implementerLokasiHaiwanStatusCode: 21,
          });
        if (!result) return;
        else console.log(result);
      }
    }

    // result = await knex
    //   .connect(`user`)
    //   .update({
    //     userFullname: fullname,
    //   })
    //   .where(`userUsername`, username);

    status = "berjaya";
    message = "masuk dah dalam db";

    // console.log(result);
  } catch (error) {
    status = "unsuccess";
    message = "API error. " + error;
    console.log(error);
  }

  return result;
}

async function updatesetupKorbanImplementerLokasi(
  implementerId,
  updateImplementerLokasi,
  setupKorbanId
) {
  let result = null;

  try {
    result = await knex
      .connect(`setupKorbanImplementerLokasi`)
      .update({
        implementerId: implementerId,
        setupKorbanId: "1",
      })
      .where(`implementerId`, implementerId);
    console.log(result);
  } catch (error) {
    console.log(error);
  }

  return result;
}

//DELETE
async function deleteUser(username) {
  let result = null;

  try {
    result = await knex.connect(`user`).where({ userUsername: username }).del();

    console.log(result);
  } catch (error) {
    console.log(error);
  }

  return result;
}

module.exports = {
  getCurrentSetupKorbanId,
  insertImplementerLokasi,
  insertImplementer,
  insertImplementerLokasiHaiwan,
  insertsetupKorbanImplementerLokasi,
  getImplementerButiran,
  displayImplementer,
  checkUsernameEmail,
  checkUsername,
  checkEmail,
  checkPhone,
  updateImplementer,
  updateImplementerLokasi,
  updateImplementerLokasiHaiwan,
  updatesetupKorbanImplementerLokasi,
  deleteUser,
};
