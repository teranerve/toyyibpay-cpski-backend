const knex = require("../../connection.js");
const moment = require("moment");
const axios = require("axios");
const sgMail = require("@sendgrid/mail");

async function getAktivitiLookup() {
  let result = null;

  result = await knex
    .connect(`aktivitiLookup`)
    .select(`aktivitiLookupId`, `aktivitiLookupTajuk`)
    .where("aktivitiLookupStatus", "Active");
  console.log(result);

  return result;
}

// ADJUSTMENT ON 26-6-2022 : 13:00 PM
async function getAktiviti(implementerId) {
  let result = null;
  let name = null;

  result = await knex
    .connect(`setupKorbanAktiviti`)
    // .join(`aktivitiDate`, `setupKorbanAktiviti.setupKorbanAktivitiId`, `aktivitiDate.setupKorbanAktivitiId`)
    .join(
      `aktivitiLookup`,
      `setupKorbanAktiviti.aktivitiLookupId`,
      `aktivitiLookup.aktivitiLookupId`
    )
    .where(`setupKorbanAktiviti.implementerId`, implementerId);

  name = await knex
    .connect("implementer")
    .select("implementerFullName")
    .where("implementerId", implementerId);

  for (let i = 0; i < result.length; i++) {
    // let countRow = await knex
    //   .connect(`setupKorbanAktiviti`)
    //   .join(
    //     `aktivitiDate`,
    //     `setupKorbanAktiviti.setupKorbanAktivitiId`,
    //     '=',
    //     `aktivitiDate.setupKorbanAktivitiId`,
    //   )
    //   .join(
    //     `aktivitiDateLivestok`,
    //     `aktivitiDate.aktivitiDateId`,
    //     '=',
    //     `aktivitiDateLivestok.aktivitiDateId`,
    //   )
    //   .join(
    //     `aktivitiLookup`,
    //     `setupKorbanAktiviti.aktivitiLookupId`,
    //     '=',
    //     `aktivitiLookup.aktivitiLookupId`,
    //   )
    //   .count('* as count')
    //   .where({
    //     'setupKorbanAktiviti.setupKorbanAktivitiId':
    //       result[i].setupKorbanAktivitiId,
    //     'setupKorbanAktiviti.implementerId': result[i].implementerId,
    //   })

    // let countRow = await knex.connect.raw(
    //   `SELECT count(DISTINCT liveStokId) AS count FROM setupKorbanAktiviti
    // JOIN aktivitiDate ON setupKorbanAktiviti.setupKorbanAktivitiId = aktivitiDate.setupKorbanAktivitiId
    // JOIN aktivitiDateLivestok ON aktivitiDate.aktivitiDateId = aktivitiDateLivestok.aktivitiDateId
    // JOIN aktivitiLookup ON setupKorbanAktiviti.aktivitiLookupId = aktivitiLookup.aktivitiLookupId
    // WHERE setupKorbanAktiviti.setupKorbanAktivitiId = ` +
    //     result[i].setupKorbanAktivitiId +
    //     `
    // AND setupKorbanAktiviti.implementerId = ` +
    //     result[i].implementerId,
    // )

    let countRow = await knex.connect.raw(
      `SELECT count(DISTINCT liveStokId) AS count FROM setupKorbanAktiviti
        JOIN aktivitiDate ON setupKorbanAktiviti.setupKorbanAktivitiId = aktivitiDate.setupKorbanAktivitiId
        JOIN aktivitiDateLivestok ON aktivitiDate.aktivitiDateId = aktivitiDateLivestok.aktivitiDateId
        JOIN aktivitiLookup ON setupKorbanAktiviti.aktivitiLookupId = aktivitiLookup.aktivitiLookupId
        WHERE setupKorbanAktiviti.setupKorbanAktivitiId = ` +
        result[i].setupKorbanAktivitiId +
        `
        AND setupKorbanAktiviti.implementerId = ` +
        result[i].implementerId +
        `
        AND aktivitiDateLivestok.aktivitiDateLivestokStatus = 'ACTIVE'`
    );

    result[i].jumlahHaiwan = countRow[0][0].count;
  }

  return [result, name];
}

async function getAktivitiDetails(setupKorbanAktivitiId) {
  let result = null;
  let date = null;
  let jumlahStokHaiwanArray = [];

  result = await knex
    .connect(`setupKorbanAktiviti`)
    .where(`setupKorbanAktivitiId`, setupKorbanAktivitiId);

  date = await knex
    .connect(`aktivitiDate`)
    .where(`setupKorbanAktivitiId`, setupKorbanAktivitiId);

  for (let i = 0; i < date.length; i++) {
    jumlahStokHaiwan = await knex
      .connect(`aktivitiDate`)
      .join(
        "aktivitiDateLivestok",
        "aktivitiDate.aktivitiDateId",
        "=",
        "aktivitiDateLivestok.aktivitiDateId"
      )
      .count("* as count")
      .where(`aktivitiDate.aktivitiDateId`, date[i].aktivitiDateId)
      .where(`aktivitiDateLivestok.aktivitiDateLivestokStatus`, `ACTIVE`);

    jumlahStokHaiwanArray.push({
      count: jumlahStokHaiwan[0].count,
    });
  }

  return [result, date, jumlahStokHaiwanArray];
}

async function getAktivitiStokHaiwan(aktivitiDateId) {
  let result = null;

  /**
     *  SELECT
      liveStok.liveStokId,
      liveStok.liveStokNo,
      liveStok.liveStokType,
      liveStok.liveStokIbadah,
      aktivitiDateLivestok.aktivitiDateLivestokStatus
  FROM
      aktivitiDate
      JOIN aktivitiDateLivestok ON aktivitiDate.aktivitiDateId = aktivitiDateLivestok.aktivitiDateId
      JOIN liveStok ON aktivitiDateLivestok.liveStokId = liveStok.liveStokId
  WHERE
      aktivitiDate.aktivitiDateId = 97
     */

  result = await knex
    .connect(`aktivitiDate`)
    .join(
      `aktivitiDateLivestok`,
      `aktivitiDate.aktivitiDateId`,
      `=`,
      `aktivitiDateLivestok.aktivitiDateId`
    )
    .join(
      `liveStok`,
      `aktivitiDateLivestok.liveStokId`,
      `=`,
      `liveStok.liveStokId`
    )
    .select(
      `liveStok.liveStokId`,
      `liveStok.liveStokNo`,
      `liveStok.liveStokType`,
      `liveStok.liveStokIbadah`,
      `aktivitiDateLivestok.aktivitiDateLivestokStatus`
    )
    .where(`aktivitiDate.aktivitiDateId`, aktivitiDateId);

  return result;
}

async function getImplementerById(implementerId) {
  let result = null;

  result = await knex
    .connect(`implementer`)
    .join(
      "implementerLokasi",
      "implementerLokasi.implementerId",
      "=",
      "implementer.implementerId"
    )
    .where(`implementer.implementerId`, implementerId)
    .select("implementer.*", "implementerLokasi.implementerLokasiId");

  console.log(result);
  return result;
}

async function getSetupKorbanId(year) {
  let result = null;

  result = await knex
    .connect(`setupKorban`)
    .where(`setupKorbanTahun`, year)
    .select("setupKorbanId");

  console.log(result);
  return result;
}

async function insertSetupKorbanAktiviti(
  aktivitiLookupId,
  implementerId,
  implementerLokasiId,
  ibadahType,
  setupKorbanAktivitiDescription,
  setupKorbanId,
  setupKorbanAktivitiStatus,
  setupKorbanAktivitiStatusCode
) {
  let result = null;

  try {
    let sql = await knex.connect(`setupKorbanAktiviti`).insert({
      aktivitiLookupId: aktivitiLookupId,
      implementerId: implementerId,
      implementerLokasiId: implementerLokasiId,
      ibadahType: ibadahType,
      aktivitiDescription: setupKorbanAktivitiDescription,
      setupKorbanId: setupKorbanId,
      setupKorbanAktivitiStatus: setupKorbanAktivitiStatus,
      setupKorbanAktivitiStatusCode: setupKorbanAktivitiStatusCode,
    });
    console.log("setupKorbanAktiviti: ", sql[0]);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function insertAktivitDate(
  setupKorbanAktivitiId,
  aktivitiLookupId,
  implementerId,
  aktivitiDateTarikh,
  aktivitiDateStatus,
  aktivitiDateStatusCode
) {
  // insertSetupKorbanAktiviti,
  // aktivitiLookupId,
  // implementerId,
  // aktivitiDateTarikh,
  // aktivitiDateTarikh,
  // statusAktiviti,
  // setupKorbanAktivitiStatusCode
  let result = null;
  let sql = null;

  try {
    for (let i = 0; i < aktivitiDateTarikh.length; i++) {
      sql = await knex.connect(`aktivitiDate`).insert({
        setupKorbanAktivitiId: setupKorbanAktivitiId,
        aktivitiLookupId: aktivitiLookupId,
        implementerId: implementerId,
        aktivitiDateTarikhPelaksanaan: aktivitiDateTarikh[i].tarikh,
        aktivitiDateTarikhNotifikasi: aktivitiDateTarikh[i].tarikhNotifikasi,
        aktivitiDateCreated: moment().format("YYYY-MM-DD HH:mm:ss"),
        aktivitiDateStatus: aktivitiDateStatus,
        aktivitiDateStatusCode: aktivitiDateStatusCode,
      });
      console.log("insertAktivitiDate: ", sql[0]);
    }

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function updateAktivitDate(
  aktivitiDateId,
  aktivitiDateTarikh,
  aktivitiDateStatus,
  aktivitiDateStatusCode
) {
  // insertSetupKorbanAktiviti,
  // aktivitiLookupId,
  // implementerId,
  // aktivitiDateTarikh,
  // aktivitiDateTarikh,
  // statusAktiviti,
  // setupKorbanAktivitiStatusCode
  let result = null;
  let sql = null;

  try {
    sql = await knex
      .connect(`aktivitiDate`)
      .where("aktivitiDateId", "=", aktivitiDateId)
      .update({
        // setupKorbanAktivitiId: setupKorbanAktivitiId,
        // aktivitiLookupId: aktivitiLookupId,
        // implementerId: implementerId,
        aktivitiDateTarikhPelaksanaan: aktivitiDateTarikh,
        // aktivitiDateTarikhNotifikasi: aktivitiDateTarikh[i].tarikhNotifikasi,
        // aktivitiDateCreated: moment().format('YYYY-MM-DD HH:mm:ss'),
        aktivitiDateStatus: aktivitiDateStatus,
        aktivitiDateStatusCode: aktivitiDateStatusCode,
      });

    console.log("updateAktivitDate: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

// ADJUSTMENT ON 26-6-2022 : 18:00 PM
async function getTarikhAktiviti(setupKorbanAktivitiId) {
  let result = null,
    res2 = null;
  let name = null;

  //   result = await knex
  //     .connect(`aktivitiDate`)
  //     .join(
  //       'setupKorbanAktiviti',
  //       'aktivitiDate.setupKorbanAktivitiId',
  //       '=',
  //       'setupKorbanAktiviti.setupKorbanAktivitiId',
  //     )
  //     .join(
  //       'aktivitiLookup',
  //       'aktivitiDate.aktivitiLookupId',
  //       '=',
  //       'aktivitiLookup.aktivitiLookupId',
  //     )
  //     .leftJoin(
  //       'aktivitiDateLivestok',
  //       'aktivitiDate.aktivitiDateId',
  //       '=',
  //       'aktivitiDateLivestok.aktivitiDateId',
  //     )
  //     .where(`aktivitiDate.setupKorbanAktivitiId`, setupKorbanAktivitiId)
  //     .count('aktivitiDateLivestok.aktivitiDateId as count')
  //     .select(`aktivitiDate.*`, `aktivitiLookup.aktivitiLookupTajuk`)
  //     .groupBy('aktivitiDate.aktivitiDateId')

  //   result = await knex.connect.raw(
  //     `SELECT count(distinct(aktivitiDateLivestok.liveStokId)) as count,
  //           IFNULL(aktivitiDateLivestok.implementerId, aktivitiDate.implementerId) as implementerId,
  //           aktivitiLookup.aktivitiLookupTajuk, aktivitiDate.aktivitiDateStatus,
  //           aktivitiDate.aktivitiDateId, aktivitiDate.aktivitiDateTarikhPelaksanaan,
  //           aktivitiDateLivestok.aktivitiDateLivestokStatus
  //           FROM aktivitiDate
  //           JOIN setupKorbanAktiviti ON aktivitiDate.setupKorbanAktivitiId = setupKorbanAktiviti.setupKorbanAktivitiId
  //           JOIN aktivitiLookup ON aktivitiDate.aktivitiLookupId = aktivitiLookup.aktivitiLookupId
  //           LEFT JOIN aktivitiDateLivestok ON aktivitiDate.aktivitiDateId = aktivitiDateLivestok.aktivitiDateId
  //           WHERE aktivitiDate.setupKorbanAktivitiId = ` +
  //       setupKorbanAktivitiId +
  //       `
  //           GROUP BY aktivitiDate.aktivitiDateId, aktivitiDateLivestok.implementerId,
  //           aktivitiDate.aktivitiDateStatus, aktivitiDate.aktivitiDateId,
  //           aktivitiDate.aktivitiDateTarikhPelaksanaan, aktivitiDateLivestok.aktivitiDateLivestokStatus`,
  //   )

  //   result = await knex.connect.raw(
  //     `SELECT count(distinct(aktivitiDateLivestok.liveStokId)) as count,
  //         IFNULL(aktivitiDateLivestok.implementerId, aktivitiDate.implementerId) as implementerId,
  //         aktivitiLookup.aktivitiLookupTajuk, aktivitiDate.aktivitiDateStatus,
  //         aktivitiDate.aktivitiDateId, aktivitiDate.aktivitiDateTarikhPelaksanaan,
  //         aktivitiDateLivestok.aktivitiDateLivestokStatus
  //         FROM aktivitiDate
  //         JOIN setupKorbanAktiviti ON aktivitiDate.setupKorbanAktivitiId = setupKorbanAktiviti.setupKorbanAktivitiId
  //         JOIN aktivitiLookup ON aktivitiDate.aktivitiLookupId = aktivitiLookup.aktivitiLookupId
  //         LEFT JOIN aktivitiDateLivestok ON aktivitiDate.aktivitiDateId = aktivitiDateLivestok.aktivitiDateId
  //         WHERE aktivitiDate.setupKorbanAktivitiId = ` +
  //       setupKorbanAktivitiId +
  //       `
  //         AND aktivitiDateLivestok.aktivitiDateLivestokStatus = 'ACTIVE'
  //         GROUP BY aktivitiDate.aktivitiDateId, aktivitiDateLivestok.implementerId,
  //         aktivitiDate.aktivitiDateStatus, aktivitiDate.aktivitiDateId,
  //         aktivitiDate.aktivitiDateTarikhPelaksanaan, aktivitiDateLivestok.aktivitiDateLivestokStatus
  //     UNION
  //     SELECT count(distinct(aktivitiDateLivestok.liveStokId)) as count,
  //         IFNULL(aktivitiDateLivestok.implementerId, aktivitiDate.implementerId) as implementerId,
  //         aktivitiLookup.aktivitiLookupTajuk, aktivitiDate.aktivitiDateStatus,
  //         aktivitiDate.aktivitiDateId, aktivitiDate.aktivitiDateTarikhPelaksanaan,
  //         aktivitiDateLivestok.aktivitiDateLivestokStatus
  //         FROM aktivitiDate
  //         JOIN setupKorbanAktiviti ON aktivitiDate.setupKorbanAktivitiId = setupKorbanAktiviti.setupKorbanAktivitiId
  //         JOIN aktivitiLookup ON aktivitiDate.aktivitiLookupId = aktivitiLookup.aktivitiLookupId
  //         LEFT JOIN aktivitiDateLivestok ON aktivitiDate.aktivitiDateId = aktivitiDateLivestok.aktivitiDateId
  //         WHERE aktivitiDate.setupKorbanAktivitiId = ` +
  //       setupKorbanAktivitiId +
  //       `
  //         AND ISNULL(aktivitiDateLivestok.aktivitiDateLivestokStatus)
  //         GROUP BY aktivitiDate.aktivitiDateId, aktivitiDateLivestok.implementerId,
  //         aktivitiDate.aktivitiDateStatus, aktivitiDate.aktivitiDateId,
  //         aktivitiDate.aktivitiDateTarikhPelaksanaan, aktivitiDateLivestok.aktivitiDateLivestokStatus`,
  //   )

  result = await knex.connect.raw(
    `select a.aktivitiDateId, a.aktivitiDateTarikhPelaksanaan,
    a.aktivitiDateStatus,
    (SELECT COUNT(DISTINCT b.liveStokId) from aktivitiDateLivestok b
    where b.aktivitiDateId = a.aktivitiDateId
    and b.aktivitiDateLivestokStatus = "Active") count, a.implementerId, z.aktivitiLookupTajuk
    from aktivitiDate a
    join aktivitiLookup z on a.aktivitiLookupId = z.aktivitiLookupId
    where a.aktivitiDateStatus = "Active"
    and a.setupKorbanAktivitiId = ` + setupKorbanAktivitiId
  );
  console.log("result: ", result[0]);

  name = await knex
    .connect("implementer")
    .select("implementerFullName", "implementerNegara", "implementerKawasan")
    .where("implementerId", result[0][0].implementerId);

  // console.log('result');
  // console.log(result);
  // console.log(name);

  return [result, name, result[0][0].implementerId];
}

// ISMAT
async function getAktivitiWithDate(implementerId) {
  let result = null;

  result = await knex.connect.raw(
    `SELECT * FROM setupKorbanAktiviti
        JOIN aktivitiLookup ON setupKorbanAktiviti.aktivitiLookupId = aktivitiLookup.aktivitiLookupId
        JOIN aktivitiDate ON setupKorbanAktiviti.setupKorbanAktivitiId = aktivitiDate.setupKorbanAktivitiId
        WHERE setupKorbanAktiviti.implementerId = ` + implementerId
  );

  return result;
}

async function getSenaraiStokTarikh(Id) {
  let result = null;

  // result = await knex.connect.raw(
  //   `select distinct c.liveStokType JENISHAIWAN, c.liveStokNo SNHAIWAN,
  //   a.liveStokId, a.implementerId, a.implementerLokasiId, a.tempahanDetailsId
  //   from tempahanDetailsLiveStok a
  //   join tempahan b on a.tempahanId = b.tempahanId
  //   join liveStok c on a.liveStokId = c.liveStokId
  //   WHERE b.tempahanStatus = "Success"
  //   AND a.implementerId = ` +
  //     Id +
  //     `
  //   AND ISNULL(a.dateAktivitiLivestokId)
  //   AND ISNULL(a.setupKorbanAktivitiId)
  //   AND ISNULL(a.dateAktivitiId)
  //   AND c.liveStokIbadah = "Korban" order by a.liveStokId ASC`,
  // )

  result = await knex.connect.raw(
    `select distinct a.liveStokId, c.liveStokNo STOKHAIWAN,
    c.liveStokType JENISHAIWAN, c.liveStokBilBahagian - c.liveStokBilBahagianBaki BILANGANTERJUAL, a.implementerId, a.implementerLokasiId
    from tempahanDetailsLiveStok a
    join tempahan b on a.tempahanId = b.tempahanId
    join liveStok c on a.liveStokId = c.liveStokId
    left join aktivitiDateLivestok d on c.liveStokId = d.liveStokId
    WHERE b.tempahanStatus = "Success" and 
    a.implementerId = ` +
      Id +
      `
    and d.liveStokId is null 
    order by a.liveStokId ASC;`
  );

  //   result = await knex.connect.raw(
  //     `select distinct c.liveStokType JENISHAIWAN, c.liveStokNo SNHAIWAN,
  //     a.liveStokId, a.implementerId, a.implementerLokasiId, d.aktivitiDateLivestokStatus
  //     from tempahanDetailsLiveStok a
  //     join tempahan b on a.tempahanId = b.tempahanId
  //     join liveStok c on a.liveStokId = c.liveStokId
  //     join aktivitiDateLivestok d on a.tempahanDetailsId
  //     WHERE b.tempahanStatus = "Success"
  //     AND a.implementerId = ` + Id + `
  //     AND c.liveStokIbadah = "Korban"
  //     order by a.liveStokId ASC`
  //   )

  return result;
}

async function getSenaraiStok(Id, implementer) {
  let result = null;

  result = await knex.connect.raw(
    `SELECT
          liveStok.liveStokId,
          liveStok.liveStokNo,
          liveStok.liveStokType,
          aktivitiDateLivestok.tempahanDetailsId,
          aktivitiDateLivestok.implementerId,
          aktivitiDateLivestok.implementerLokasiId,
          aktivitiDateLivestok.aktivitiDateLivestokStatus,
          aktivitiDateLivestok.aktivitiDateLivestokId,
          aktivitiDateLivestok.setupKorbanAktivitiId
      FROM
          tempahanDetailsLiveStok
          JOIN tempahanDetails ON tempahanDetailsLiveStok.tempahanDetailsId = tempahanDetails.tempahanDetailsId
          JOIN liveStok ON tempahanDetailsLiveStok.liveStokId = liveStok.liveStokId
          JOIN liveStokBahagian ON liveStok.liveStokId = liveStokBahagian.liveStokId
          JOIN aktivitiDateLivestok ON liveStokBahagian.liveStokId = aktivitiDateLivestok.liveStokId
      WHERE
          tempahanDetailsLiveStok.implementerId = ` +
      implementer +
      `   AND aktivitiDateLivestok.aktivitiDateLivestokStatus = 'ACTIVE'
          AND tempahanDetails.tempahanDetailsStatus = 'Success'
          AND aktivitiDateLivestok.aktivitiDateId = ` +
      Id +
      `
      GROUP BY
          liveStok.liveStokId,
          liveStok.liveStokNo,
          liveStok.liveStokType,
          aktivitiDateLivestok.tempahanDetailsId,
          aktivitiDateLivestok.implementerId,
          aktivitiDateLivestok.implementerLokasiId,
          aktivitiDateLivestok.aktivitiDateLivestokStatus,
          aktivitiDateLivestok.aktivitiDateLivestokId`
  );

  return result;
}

async function getAktivitiDate(Id) {
  let result = null;

  result = await knex.connect.raw(
    `SELECT setupKorbanAktivitiId
      FROM aktivitiDate
      WHERE aktivitiDateId = ` + Id
  );

  return result;
}

async function getImplementerAktivitiDate(aktivitiDateId) {
  let result = null;

  result = await knex.connect.raw(
    `SELECT implementer.implementerId, implementer.implementerFullname, implementer.implementerKawasan, 
    implementer.implementerNegara, aktivitiLookup.aktivitiLookupTajuk, setupKorbanAktiviti.ibadahType,
    aktivitiDate.aktivitiDateTarikhPelaksanaan, setupKorbanAktiviti.setupKorbanAktivitiId
      FROM aktivitiDate JOIN implementer ON aktivitiDate.implementerId = implementer.implementerId
      JOIN setupKorbanAktiviti ON aktivitiDate.setupKorbanAktivitiId = setupKorbanAktiviti.setupKorbanAktivitiId
      JOIN aktivitiLookup ON setupKorbanAktiviti.aktivitiLookupId = aktivitiLookup.aktivitiLookupId
      WHERE aktivitiDate.aktivitiDateId = ` + aktivitiDateId
  );

  return result;
}

async function getAktivitiDateLivestok(Id, type) {
  let result = null;

  if (type == "display") {
    result = await knex.connect.raw(
      `SELECT * FROM aktivitiDateLivestok
        JOIN liveStok ON aktivitiDateLivestok.liveStokId = liveStok.liveStokId
        WHERE aktivitiDateLivestok.aktivitiDateId = ` + Id
    );
  } else if (type == "get-tempahanDetailsId") {
    result = await knex.connect.raw(
      `SELECT tempahanDetailsLiveStok.liveStokId, tempahanDetailsLiveStok.liveStokBahagianId, 
      tempahanDetailsLiveStok.tempahanId, tempahanDetailsLiveStok.tempahanDetailsId
      FROM aktivitiDateLivestok
      JOIN tempahanDetails ON aktivitiDateLivestok.tempahanDetailsId = tempahanDetails.tempahanDetailsId
      JOIN tempahanDetailsLiveStok ON tempahanDetails.tempahanId = tempahanDetailsLiveStok.tempahanId
      WHERE aktivitiDateLivestok.aktivitiDateLivestokId =` + Id
    );
  }

  return result;
}

async function insertAktivitiDateLivestok(
  adID,
  lsID,
  tdID,
  skaID,
  implementerID,
  implementerLokasiID,
  status
) {
  let result = null,
    getData = null;
  let sql = null;

  try {
    getData = await knex.connect.raw(
      `SELECT *
        FROM aktivitiDateLivestok
        WHERE aktivitiDateId = ` +
        adID +
        `
        AND liveStokId = ` +
        lsID +
        `
        AND tempahanDetailsId = ` +
        tdID +
        `
        AND setupKorbanAktivitiId = ` +
        skaID
    );
    console.log(`lolzzzz: ` + JSON.stringify(getData[0].length));

    if (getData[0].length == 0) {
      sql = await knex.connect(`aktivitiDateLivestok`).insert({
        aktivitiDateId: adID,
        liveStokId: lsID,
        tempahanDetailsId: tdID,
        setupKorbanAktivitiId: skaID,
        implementerId: implementerID,
        implementerLokasiId: implementerLokasiID,
        aktivitiDateLivestokStatus: status,
      });

      console.log("insertAktivitiDateLivestok: ", sql);

      if (!sql || sql.length == 0) {
        result = false;
      } else {
        result = sql[0];
      }
    } else {
      result = false;
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function getBulktempahanDetailsLiveStok(implementer, livestok) {
  let result = null;

  result = await knex.connect.raw(
    `select a.tempahanDetailsId, a.tempahanId, a.liveStokId
    from tempahanDetailsLiveStok a
    join tempahan b on a.tempahanId = b.tempahanId
    join liveStok c on a.liveStokId = c.liveStokId
    join tempahanDetails d on a.tempahanDetailsId = d.tempahanDetailsId
    WHERE b.tempahanStatus = "Success" 
    AND a.implementerId = ` +
      implementer +
      `
    AND a.liveStokId = ` +
      livestok +
      `
    group by a.tempahanDetailsId, a.tempahanId`
  );

  return result;
}

async function updateTempahanDetailsLiveStok(
  tempahan,
  liveStok,
  liveStokBhg,
  dalID,
  skaID,
  daID,
  tempahanDetailsID
) {
  let result = null;
  let sql = null;

  try {
    sql = await knex
      .connect(`tempahanDetailsLiveStok`)
      .where("tempahanId", "=", tempahan)
      .where("liveStokId", "=", liveStok)
      .where("tempahanDetailsId", "=", tempahanDetailsID)
      .update({
        dateAktivitiLivestokId: dalID,
        setupKorbanAktivitiId: skaID,
        dateAktivitiId: daID,
      });

    console.log("updateTempahanDetailsLiveStok: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function updateAktivitiDateLivestok(aktivitiLiveStok, status) {
  let result = null;
  let sql = null;

  try {
    sql = await knex
      .connect(`aktivitiDateLivestok`)
      .where("aktivitiDateLivestokId", "=", aktivitiLiveStok)
      .update({
        aktivitiDateLivestokStatus: status,
      });

    console.log("updateAktivitiDateLivestok: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function getSelectedWakil(Id) {
  let result = null;

  result = await knex.connect.raw(
    `SELECT user.userFullname, user.userPhoneNo, user.userEmail FROM tempahanDetails
    JOIN tempahan ON tempahanDetails.tempahanId = tempahan.tempahanId
    JOIN userWakil ON tempahan.userWakilId = userWakil.userWakilId
    JOIN user ON userWakil.userId = user.userId
    WHERE tempahanDetailsStatus = 'Success'
    AND tempahanDetailsId = ` + Id
  );

  return result;
}

async function getNotificationCount(Id) {
  let result = null;

  result = await knex.connect.raw(
    //     `SELECT count(*) AS counter, notificationType
    //   FROM notification
    //   WHERE notificationRefId = ` + Id + `
    //   GROUP BY notificationType`

    `SELECT count(*) AS counter
      FROM notification 
      WHERE notificationRefId = ` +
      Id +
      `
      `
  );

  return result[0];
}

async function getNotificationLog(Id, notificationRefId) {
  let result = null;

  result = await knex.connect.raw(
    `SELECT *  
        FROM notification
        JOIN aktivitiDateLivestok ON notification.notificationRefId = aktivitiDateLivestok.aktivitiDateLivestokId
        JOIN tempahanDetails ON aktivitiDateLivestok.tempahanDetailsId = tempahanDetails.tempahanDetailsId
        JOIN tempahan ON tempahanDetails.tempahanId = tempahan.tempahanId
        JOIN liveStok ON aktivitiDateLivestok.liveStokId = liveStok.liveStokId
        WHERE notification.notificationRefTable = 'aktivitiDateLivestok'
        AND aktivitiDateLivestok.implementerId =` +
      Id +
      ` AND notification.notificationRefId = ` +
      notificationRefId
  );

  return result;
}

async function insertNotificationAktiviti(
  organization,
  receiver,
  subject,
  content,
  type,
  aktivitiDateLivestokId,
  status
) {
  let result = null;
  let sql = null;
  let date = moment().format("YYYY-MM-DD HH:mm:ss"),
    RefTable = "aktivitiDateLivestok";

  try {
    sql = await knex.connect(`notification`).insert({
      organizationId: organization,
      notificationReceiver: receiver,
      notificationSubject: subject ? subject : null,
      notificationContent: content,
      notificationType: type,
      notificationRefTable: RefTable,
      notificationRefId: aktivitiDateLivestokId,
      notificationSentDate: date,
      notificationStatus: status,
    });

    console.log("insertNotificationAktiviti: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function sendNotificationAktiviti(
  modalEmelWakil,
  modalNamaWakil,
  modalTelefonWakil,
  whatsApp,
  SMS,
  EmailSubject,
  EmailText,
  haiwanSN,
  checkSMS,
  checkWS,
  checkEmail
) {
  let result = null;
  let resultSMS = null;
  let resultWhatsapp = null;
  let resultEmail = null;
  let userNumber = "+6" + modalTelefonWakil;
  let userMessage = null;
  let userWhatsapp = null;
  let userEmailSubject = null;
  let userEmailText = null;

  userMessage = SMS;
  userWhatsapp = whatsApp;
  userEmailSubject = EmailSubject;
  userEmailText = EmailText;

  // HANTAR NOTIFIKASI MELALUI SMS
  if (checkSMS === 1) {
    await axios
      .post(
        "https://manage.smsniaga.com/api/send",
        {
          body: userMessage,
          phones: [userNumber],
          sender_id: "YAYASAN IKHLAS",
        },
        {
          headers: {
            Authorization: `Bearer ` + process.env.TOKEN,
          },
        }
      )
      .then(function (response) {
        resultSMS = response.statusText;
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  if (checkWS === 1) {
    // HANTAR NOTIFIKASI MELALUI WHATSAPP
    await axios
      .post(
        `https://api.maytapi.com/api/` +
          process.env.PRODUCT_ID +
          `/` +
          process.env.PHONE_ID +
          `/sendMessage`,
        {
          message: userWhatsapp,
          to_number: userNumber,
          type: "text",
        },
        {
          headers: {
            "Content-Type": "application/json",
            "x-maytapi-key": process.env.TOKEN_WHATSAPP,
          },
        }
      )
      .then(function (response) {
        resultWhatsapp = response.statusText;
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  if (checkEmail === 1) {
    // SEND EMAIL
    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    const msg = {
      to: modalEmelWakil,
      from: {
        email: "marketing.ikhlas@gmail.com",
        name: "Yayasan Ikhlas",
      },
      subject: userEmailSubject,
      html: userEmailText,
    };
    await sgMail
      .send(msg)
      .then(function (response) {
        resultEmail = response[0].statusCode;
      })
      .catch(function (error) {
        console.error(error);
      });
  }

  result = {
    resultSMS: resultSMS ? resultSMS : "Tiada pilihan.",
    resultWhatsapp: resultWhatsapp ? resultWhatsapp : "Tiada pilihan.",
    resultEmail: resultEmail ? resultEmail : "Tiada pilihan.",
  };

  return result;
}

async function updateAktivitiDateLivestokStatus(Id) {
  let result = null;
  let sql = null;

  try {
    sql = await knex
      .connect(`aktivitiDateLivestok`)
      .where("tempahanDetailsId", "=", Id)
      .update({
        aktivitiDateLivestokStatus: "TRANSFER",
      });

    console.log("updateAktivitiDateLivestokStatus: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

async function sendNotificationPindahStok(
  type,
  modalEmelWakil,
  modalNamaWakil,
  modalTelefonWakil,
  haiwanSN,
  tempahanNo,
  bahagianHaiwan,
  jenisHaiwan,
  currentAktivitiStatus,
  tarikh,
  ekorPeserta
) {
  let result = null;
  let resultSMS = null;
  let resultWhatsapp = null;
  let userNumber = "+6" + modalTelefonWakil;
  let userMessage = null;
  let userWhatsapp = null;
  let ringkasanMessage = "",
    ringkasanMessagev2 = "",
    date = "";

  if (type == "Ekor") {
    for (let i = 0; i < haiwanSN.length; i++) {
      ringkasanMessage += `\r\n*` + parseInt(i + 1) + `.* ` + haiwanSN[i];
    }

    ringkasanMessagev2 = ekorPeserta[0];
  } else if (type == "Bahagian") {
    for (let i = 0; i < haiwanSN.length; i++) {
      ringkasanMessage +=
        `\r\n*` +
        parseInt(i + 1) +
        `.* ` +
        haiwanSN[i].peserta +
        `, ` +
        haiwanSN[i].liveStokBahagianNo +
        `, ` +
        haiwanSN[i].ibadah +
        `, ` +
        haiwanSN[i].tempahanDetailsHaiwanType +
        `, ` +
        haiwanSN[i].lokasi +
        ` (Cadangan Pelaksanaan Korban: ` +
        haiwanSN[i].tarikh +
        `)`;

      ringkasanMessagev2 += haiwanSN[i].peserta + `, `;
    }
  }

  if (
    currentAktivitiStatus == "Kemaskini nombor haiwan" ||
    currentAktivitiStatus == "sudah berdaftar"
  ) {
    userMessage =
      "Terkini, nombor haiwan Korban/Akikah anda telah dikemas kini. Sila layari https://app.korbanikhlas.com/semak-status";
    userWhatsapp =
      `Assalammualaikum Tuan/Puan\r\n` +
      modalNamaWakil +
      `\r\n\r\nNombor haiwan korban/akikah ` +
      modalNamaWakil +
      ` telah pun dikemas kini.
        \r\n=========================
        \r\nNombor haiwan anda boleh dirujuk pada 5 digit terakhir, contoh: LDSB"00003"
        \r\nBerikut adalah ringkasan status haiwan korban anda:
        \r\n*No. Rujukan :* ` +
      tempahanNo +
      `\r\n*Nombor Haiwan :* ` +
      ringkasanMessage +
      `\r\n\r\n=========================
        \r\nTerima kasih kerana bersama Korban Ikhlas pada tahun ini.
        \r\n#korbanikhlas\r\n*Rakan Korban Terbaik Anda!*`;
  } else if (currentAktivitiStatus == "haiwan sudah disembelih") {
    userMessage =
      "Alhamdulillah anda dah boleh potong rambut dan potong kuku! Ibadah korban anda telah selesai dilaksanakan. Terima kasih!";
    userWhatsapp =
      `Selamat Hari Raya Aidiladha.
      \r\nAssalamualaikum Tuan/Puan ` +
      modalNamaWakil +
      `,
      \r\nIndah Langit Kerana Awan,\r\nIndah Laut Kerana Kebiruan,\r\nIndah Aidilfitri Kerana Kemaafan,\r\nIndah Aidiladha Kerana Pengorbanan,
      \r\nAlhamdulillah Ibadah korban ` +
      ringkasanMessagev2 +
      ` telah selesai dilaksanakan pada ` +
      tarikh +
      `. Anda sudah boleh memotong rambut dan kuku 😄.
      \r\nTerima kasih kerana terus mempercayai kami untuk menyampaikan amanah ini.
      \r\nAnda akan dapat notifikasi berkenaan E-Sijil Korban Ikhlas sebelum 31 Julai 2023.
      \r\nSemoga ia menjadi asbab bukan saja kepada kami warga Korban Ikhlas tetapi juga kepada semua pihak yang terlibat untuk mendapat pahala yang banyak di sisi Allah azza wa jalla. InsyaAllah.
      \r\n#korbanikhlas\r\n*Rakan Korban Terbaik Anda!*`;
  } else if (currentAktivitiStatus == "sijil penyertaan sudah siap") {
    userMessage =
      `Salam ` +
      modalNamaWakil +
      `, Sijil Digital korban anda telah sedia untuk dimuat turun di https://app.korbanikhlas.com/semak-status sekarang!`;

    userWhatsapp =
      `Assalamualaikum Tuan/Puan ` +
      modalNamaWakil +
      `, Makluman terkini, Sijil Digital korban ` +
      ringkasanMessagev2 +
      ` sudah siap sedia untuk dimuat turun. Sila layari https://app.korbanikhlas.com/semak-status sekarang!
      \r\nKami dari Korban Ikhlas ingin mengucapkan setinggi-tinggi penghargaan kerana memilih Korban Ikhlas sebagai rakan ibadah korban anda pada tahun 2023. 
      \r\nSaya mohon jasa baik ` +
      modalNamaWakil +
      ` untuk menyimpan nombor ini dalam 'phone contact' sebagai nombor Korban Ikhlas. 
      \r\nSaya akan maklumkan dari semasa ke semasa perkembangan terkini projek serta kempen Korban Ikhlas dijalankan sepanjang tahun.
      \r\nSemoga kita bertemu lagi Aidiladha tahun hadapan.
      \r\n=========================
      \r\nTerima Kasih Atas kesetiaan ` +
      modalNamaWakil +
      `
      \r\n#korbanikhlas\r\n*Rakan Korban Terbaik Anda!*`;
  } else {
    console.log(`NO MESSAGE`);
  }

  // HANTAR NOTIFIKASI MELALUI SMS
  await axios
    .post(
      "https://manage.smsniaga.com/api/send",
      {
        body: userMessage,
        phones: [userNumber],
        sender_id: "YAYASAN IKHLAS",
      },
      {
        headers: {
          Authorization: `Bearer ` + process.env.TOKEN,
        },
      }
    )
    .then(function (response) {
      resultSMS = response.statusText;
    })
    .catch(function (error) {
      console.log(error);
    });

  // HANTAR NOTIFIKASI MELALUI WHATSAPP
  await axios
    .post(
      `https://api.maytapi.com/api/` +
        process.env.PRODUCT_ID +
        `/` +
        process.env.PHONE_ID +
        `/sendMessage`,
      {
        message: userWhatsapp,
        to_number: userNumber,
        type: "text",
      },
      {
        headers: {
          "Content-Type": "application/json",
          "x-maytapi-key": process.env.TOKEN_WHATSAPP,
        },
      }
    )
    .then(function (response) {
      resultWhatsapp = response.statusText;
    })
    .catch(function (error) {
      console.log(error);
    });

  result = {
    resultSMS: resultSMS ? resultSMS : "Tiada pilihan.",
    resultWhatsapp: resultWhatsapp ? resultWhatsapp : "Tiada pilihan.",
    userWhatsappText: userWhatsapp ? userWhatsapp : "NO WHATSAPP",
    userMessageText: userMessage ? userMessage : "NO SMS",
  };

  return result;
}

async function getBulkStock(liveStokId) {
  let result = null;

  //   result = await knex.connect.raw(
  //     `SELECT * FROM tempahanDetailsLiveStok
  //     JOIN tempahanDetails ON tempahanDetailsLiveStok.tempahanDetailsId = tempahanDetails.tempahanDetailsId
  //     WHERE tempahanDetails.tempahanDetailsStatus = 'Success'
  //     AND tempahanDetailsLiveStok.liveStokId = ` + liveStokId,
  //   )

  result = await knex.connect.raw(
    `SELECT tempahanDetailsLiveStok.tempahanDetailsId, tempahanDetails.tempahanDetailsBhgnType, tempahanDetails.tempahanId, tempahanDetailsLiveStok.liveStokId
    FROM tempahanDetailsLiveStok
    JOIN tempahanDetails ON tempahanDetailsLiveStok.tempahanDetailsId = tempahanDetails.tempahanDetailsId
    WHERE tempahanDetails.tempahanDetailsStatus = 'Success'
    AND tempahanDetailsLiveStok.liveStokId = ` +
      liveStokId +
      `
    group by tempahanDetailsLiveStok.tempahanDetailsId, tempahanDetails.tempahanDetailsBhgnType`
  );

  return result[0];
}

async function getBulkStockEach(liveStokId) {
  let result = null;

  result = await knex.connect.raw(
    `SELECT * FROM tempahanDetailsLiveStok
      JOIN tempahanDetails ON tempahanDetailsLiveStok.tempahanDetailsId = tempahanDetails.tempahanDetailsId
      WHERE tempahanDetails.tempahanDetailsStatus = 'Success'
      AND tempahanDetailsLiveStok.liveStokId = ` + liveStokId
  );

  return result[0];
}

async function getBulkWakil(tempahanDetailsId) {
  let result = null;

  result = await knex.connect.raw(
    `SELECT user.userFullname, user.userEmail, user.userPhoneNo, liveStokBahagian.liveStokBahagianNo, 
    tempahan.tempahanNo, tempahanDetails.tempahanDetailsBhgnType, tempahanDetails.tempahanDetailsHaiwanType, 
    liveStok.liveStokIbadah, implementer.implementerNegara, wakilPeserta.wakilPesertaName
    FROM tempahanDetails
    JOIN tempahan ON tempahanDetails.tempahanId = tempahan.tempahanId
    JOIN userWakil ON tempahan.userWakilId = userWakil.userWakilId
    JOIN user ON userWakil.userId = user.userId
    JOIN tempahanDetailsLiveStok ON tempahanDetails.tempahanDetailsId = tempahanDetailsLiveStok.tempahanDetailsId
    JOIN implementerLokasi ON tempahanDetailsLiveStok.implementerLokasiId = implementerLokasi.implementerLokasiId
    JOIN implementer ON implementerLokasi.implementerId = implementer.implementerId
    JOIN liveStok ON tempahanDetailsLiveStok.liveStokId = liveStok.liveStokId
    JOIN liveStokBahagian ON tempahanDetailsLiveStok.liveStokBahagianId = liveStokBahagian.liveStokBahagianId
    JOIN wakilPeserta ON tempahanDetails.wakilPesertaId = wakilPeserta.wakilPesertaId
    WHERE tempahanDetails.tempahanDetailsId = ` + tempahanDetailsId
  );

  return result[0];
}

async function getImplementerCiptaSijil(implementerId) {
  let result = null;

  result = await knex.connect.raw(
    `SELECT 
      implementerId, 
      implementerFullname, 
      implementerKawasan, 
      implementerNegara
    FROM implementer      
    WHERE implementerId = ` + implementerId
  );

  return result;
}

async function getSuccessNotification() {
  let result = null;

  result = await knex.connect.raw(
    `SELECT * FROM notification WHERE notificationStatus LIKE '%OK%'`
  );

  return result;
}

async function getFailedNotification() {
  let result = null;

  result = await knex.connect.raw(
    `SELECT * FROM notification WHERE notificationStatus LIKE '%Tiada%'`
  );

  return result;
}

async function resendNotification(type, receiver, content) {
  let result = null;
  let resultSMS = null;
  let resultWhatsapp = null;
  let userNumber = "+6" + receiver;
  let userMessage = null;
  let userWhatsapp = null;

  if (type === "whatsApp") {
    userWhatsapp = content;
  } else if (type === "SMS") {
    userMessage = content;
  }

  // HANTAR NOTIFIKASI MELALUI SMS
  await axios
    .post(
      "https://manage.smsniaga.com/api/send",
      {
        body: userMessage,
        phones: [userNumber],
        sender_id: "YAYASAN IKHLAS",
      },
      {
        headers: {
          Authorization: `Bearer ` + process.env.TOKEN,
        },
      }
    )
    .then(function (response) {
      resultSMS = response.statusText;
    })
    .catch(function (error) {
      console.log(error);
    });

  // HANTAR NOTIFIKASI MELALUI WHATSAPP
  await axios
    .post(
      `https://api.maytapi.com/api/` +
        process.env.PRODUCT_ID +
        `/` +
        process.env.PHONE_ID +
        `/sendMessage`,
      {
        message: userWhatsapp,
        to_number: userNumber,
        type: "text",
      },
      {
        headers: {
          "Content-Type": "application/json",
          "x-maytapi-key": process.env.TOKEN_WHATSAPP,
        },
      }
    )
    .then(function (response) {
      resultWhatsapp = response.statusText;
    })
    .catch(function (error) {
      console.log(error);
    });

  result = {
    resultSMS: resultSMS ? resultSMS : "Tiada pilihan.",
    resultWhatsapp: resultWhatsapp ? resultWhatsapp : "Tiada pilihan.",
    userWhatsappText: userWhatsapp ? userWhatsapp : "NO WHATSAPP",
    userMessageText: userMessage ? userMessage : "NO SMS",
  };

  return result;
}

async function updateNotificationStatus(Id) {
  let result = null;
  let sql = null;

  try {
    sql = await knex
      .connect(`notification`)
      .where("notificationId", "=", Id)
      .update({
        notificationStatus: "OK",
        notificationSentDate: moment().format("YYYY-MM-DD HH:mm:ss"),
      });

    console.log("updateNotificationStatus: ", sql);

    if (!sql || sql.length == 0) {
      result = false;
    } else {
      result = sql[0];
    }
  } catch (error) {
    console.log(error);
  }

  return result;
}

module.exports = {
  getAktivitiLookup,
  getAktiviti,
  getAktivitiDetails,
  getImplementerById,
  getSetupKorbanId,
  insertSetupKorbanAktiviti,
  insertAktivitDate,
  updateAktivitDate,
  getTarikhAktiviti,
  getAktivitiWithDate,
  getSenaraiStokTarikh,
  getSenaraiStok,
  getAktivitiDate,
  getAktivitiDateLivestok,
  insertAktivitiDateLivestok,
  updateTempahanDetailsLiveStok,
  updateAktivitiDateLivestok,
  getSelectedWakil,
  getImplementerAktivitiDate,
  getNotificationCount,
  getNotificationLog,
  insertNotificationAktiviti,
  sendNotificationAktiviti,
  getAktivitiStokHaiwan,
  updateAktivitiDateLivestokStatus,
  sendNotificationPindahStok,
  getBulkStock,
  getBulkStockEach,
  getBulkWakil,
  getImplementerCiptaSijil,
  getBulktempahanDetailsLiveStok,
  getSuccessNotification,
  getFailedNotification,
  resendNotification,
  updateNotificationStatus,
};
