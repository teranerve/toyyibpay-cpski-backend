const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/pengguna/pengguna.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/",  async (req, res) => {
    let param = null;
    let result = null;

    try {

        // BIND PARAMETER TO VARIABLES
        param = req.query;
        year = param.Id;
    
        // GET USER FUNCTION
        let getUser = await model.getUser(year);
    
        if (getUser[0] != false && getUser[1] != false) {
          result = {
            status: "berjaya",
            message: "Anda berjaya dapatkan senarai ejen",
            environment: process.env.ENVIRONMENT,
            userdata: getUser[0],
            count: getUser[1],
          };
        } else {
          result = {
            status: "gagal",
            message: 'Anda gagal dapatkan senarai ejen'
          }
        }
      } catch (error) {
        console.log(error); // LOG ERROR
        result = {
          message: `API Error`,
        };
      }
    
      // RETURN
      res.status(200).json(result);

});

module.exports = router;