const express = require("express");
const router = express.Router();
const model = require("../../function/sijil/sijil.js");
const moment = require("moment");
const multer = require("multer");
const AWS = require("aws-sdk");
const fileUpload = require("express-fileupload");

router.use(fileUpload());

// S3 BUCKET IMAGE SETUP
const BUCKET_NAME = "assets-korbanikhlas";
const IAM_USER_KEY = "AKIAT2OILDTYNLIWRVMY"; // OLD - AKIAT2OILDTYKW54IO53
const IAM_USER_SECRET = "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"; // OLD - kP8mi386xp1yMJ/N4qGVI1k8n/f4pb/zW+bt28Xv
const BUCKET_LINK =
  "http://assets-korbanikhlas.s3-website-ap-southeast-1.amazonaws.com/sijil/";

router.post("/", async (req, res) => {
  let param = null;
  let result = null,
    img = null,
    params = null;

  try {
    if (!req.files) {
      return res.status(400).send("No files were uploaded.");
    } else {
      console.log(req.files.sijilPDF);
      param = req.body;

      console.log("--- getPesertaSijilByTempahan PROCESS START ---");
      let getPesertaSijilByTempahan = await model.getPesertaSijilByTempahan(
        param.tempahanNo,
        param.pesertaName,
        param.implementerId
      );
      console.log(
        `getPesertaSijilByTempahan: ` +
          JSON.stringify(getPesertaSijilByTempahan)
      );

      console.log("--- upload e-Sijil PDF PROCESS START ---");
      AWS.config.update({
        endpoint: "s3-ap-southeast-1.amazonaws.com",
        accessKeyId: IAM_USER_KEY,
        secretAccessKey: IAM_USER_SECRET,
        Bucket: BUCKET_NAME,
        signatureVersion: "v4",
        region: "ap-southeast-1",
      });

      const s3 = new AWS.S3();

      // Binary data base64
      const fileContent = Buffer.from(req.files.sijilPDF.data, "binary");

      console.log(
        "JENIS: ",
        getPesertaSijilByTempahan[0].tempahanDetailsBhgnType
      );

      if (getPesertaSijilByTempahan[0].tempahanDetailsBhgnType == "Ekor") {
        // Setting up S3 upload parameters
        params = {
          Bucket:
            BUCKET_NAME +
            "/sijil/" +
            process.env.ENVIRONMENT +
            "/" +
            getPesertaSijilByTempahan[0].liveStokNo,
          Key:
            param.pesertaName +
            "-" +
            getPesertaSijilByTempahan[0].liveStokNo +
            ".pdf",
          Body: fileContent,
          ContentDisposition: "inline",
          ContentType: "application/pdf",
        };

        // Uploading files to the bucket
        // s3.upload(params, function (err, data) {});

        // Uploading files to the bucket
        s3.upload(params, function (err, data) {
          if (err) {
            console.log("Error uploading file:", err);
            return false;
          }

          // Handle successful upload
          console.log("File uploaded successfully:", data);
          // Perform additional operations if needed

          // Continue with other code logic
        });

        for (let i = 0; i < getPesertaSijilByTempahan.length; i++) {
          console.log("--- updateTempahanDetailsLiveStok PROCESS START ---");
          let updateData = await model.updateTempahanDetailsLiveStok(
            getPesertaSijilByTempahan[i].tempahanDetailsLiveStokId,
            params.Key,
            getPesertaSijilByTempahan[0].liveStokNo
          );
        }
      } else if (
        getPesertaSijilByTempahan[0].tempahanDetailsBhgnType == "Bahagian"
      ) {
        for (let i = 0; i < getPesertaSijilByTempahan.length; i++) {
          // Setting up S3 upload parameters
          params = {
            Bucket:
              BUCKET_NAME +
              "/sijil/" +
              process.env.ENVIRONMENT +
              "/" +
              getPesertaSijilByTempahan[i].liveStokNo,
            Key:
              param.pesertaName +
              "-" +
              getPesertaSijilByTempahan[i].liveStokBahagianNo +
              ".pdf",
            Body: fileContent,
            ContentDisposition: "inline",
            ContentType: "application/pdf",
          };

          // Uploading files to the bucket
          s3.upload(params, function (err, data) {
            if (err) {
              console.log("Error uploading file:", err);
              return false;
            }

            // Handle successful upload
            console.log("File uploaded successfully:", data);
            // Perform additional operations if needed

            // Continue with other code logic
          });

          console.log("--- updateTempahanDetailsLiveStok PROCESS START ---");
          let updateData = await model.updateTempahanDetailsLiveStok(
            getPesertaSijilByTempahan[i].tempahanDetailsLiveStokId,
            params.Key,
            getPesertaSijilByTempahan[i].liveStokNo
          );
        }
      }

      result = {
        status: "berjaya",
        message: "ANDA BERJAYA UPLOAD KE S3 - " + params.Key,
        data: params,
      };
    }
  } catch (error) {
    console.log(error);
    result = {
      message: `API Error`,
    };
  }

  res.status(200).json(result);
});

module.exports = router;
