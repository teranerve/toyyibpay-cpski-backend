const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/liveStok/liveStok.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/",  async (req, res) => {
    let param = null;
    let result = null;

    let implementerLokasiHaiwanId = null;
    try {

        // BIND PARAMETER TO VARIABLES
        param = req.query;
        implementerLokasiHaiwanId = param.implementerLokasiHaiwanId;
    
        // GET USER FUNCTION
        const getLiveStok = await model.getLiveStok(implementerLokasiHaiwanId);
    
        if (getLiveStok[0] != false || getLiveStok[1] != false || getLiveStok[2] != false) {
          result = {
            status: "berjaya",
            message: "Anda berjaya dapatkan senarai stok haiwan.",
            data: getLiveStok[0],
            implementerLokasiHaiwan: getLiveStok[1],
            implementer: getLiveStok[2],
          };
        } else {
          result = {
            status: "gagal",
            message: 'Anda gagal dapatkan senarai stok haiwan.'
          }
        }
      } catch (error) {
        console.log(error); // LOG ERROR
        result = {
          message: `API Error`,
        };
      }
    
      // RETURN
      res.status(200).json(result);

});

module.exports = router;