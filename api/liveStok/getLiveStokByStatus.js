const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/liveStok/liveStok.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/",  async (req, res) => {
    let param = null;
    let result = null;
    let status = null;

    let implementerLokasiHaiwanId = null;
    try {

        // BIND PARAMETER TO VARIABLES
        param = req.query;
        implementerLokasiHaiwanId = param.implementerLokasiHaiwanId;
        status = param.status;
    
        // GET USER FUNCTION
        const getLiveStokByStatus = await model.getLiveStokByStatus(implementerLokasiHaiwanId, status);
    
        if (getLiveStokByStatus[0] != false || getLiveStokByStatus[1] != false || getLiveStokByStatus[2] != false) {
          result = {
            status: "berjaya",
            message: "Anda berjaya dapatkan senarai stok haiwan.",
            // data: getLiveStokByStatus,
            data: getLiveStokByStatus[0],
            implementerLokasiHaiwan: getLiveStokByStatus[1],
            implementer: getLiveStokByStatus[2],
          };
        } else {
          result = {
            status: "gagal",
            message: 'Anda gagal dapatkan senarai stok haiwan.'
          }
        }
      } catch (error) {
        console.log(error); // LOG ERROR
        result = {
          message: `API Error`,
        };
      }
    
      // RETURN
      res.status(200).json(result);

});

module.exports = router;