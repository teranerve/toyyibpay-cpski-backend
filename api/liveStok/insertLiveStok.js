const express = require("express"); // MUST HAVE
const {
  insertImplementer,
} = require("../../function/implementer/implementer.js");
const router = express.Router(); // MUST HAVE
const moment = require("moment");
const model = require("../../function/liveStok/liveStok.js"); // INCLUDE FUNCTION FILE
const conf = require("../../function/configuration/configuration.js");
// MAKE SURE METHOD IS CORRECT WHEN CALLING API

// POST USER
router.post("/", async (req, res) => {
  let param = null;
  let result = null;

  // VARIABLE SETUP
  let implementerId = null;
  let implementerLokasiId = null;
  let implementerLokasiHaiwanId = null;
  let liveStokType = null;
  let jumlahBahagian = null;
  let stokBaru = null;
  let jenisIbadah = null;

  let implementerKawasan = null;
  let implementerNegara = null;
  let organizationId = null;

  let setupKorbanId = null;
  let liveStokNo = null;
  let liveStokBahagianNo = null;
  let pad = "00000";
  let pad2 = "00";

  try {
    // BIND PARAMETER TO VARIABLES
    param = req.body;

    implementerId = param.implementerId;
    implementerLokasiId = param.implementerLokasiId;
    implementerLokasiHaiwanId = param.implementerLokasiHaiwanId;
    liveStokType = param.liveStokType;
    jumlahBahagian = param.jumlahBahagian;
    stokBaru = param.stokBaru;
    jenisIbadah = param.jenisIbadah;
    setupKorbanId = param.setupKorbanId;

    // INSERT USER EJEN

    let implementerDetails = await model.getImplementerDetails(implementerId);

    implementerKawasan = implementerDetails[0].implementerKawasan;
    implementerNegara = implementerDetails[0].implementerNegara;
    organizationId = implementerDetails[0].organizationId;
    console.log("--------------implementerKawasan: ", implementerKawasan);

    let latestLiveStokId = await model.getLatestLiveStokId();
    latestLiveStokId = latestLiveStokId[0][0].latestId;
    let idString = Number(latestLiveStokId).toString();
    console.log(
      "--------------latest livestokid: ",
      pad.substring(0, pad.length - idString.length) + idString
    );

    let getCode = await model.getCode();

    let getBilBahagian = await model.getBilBahagian();
    getBilBahagian = getBilBahagian[0];
    // console.log("--------------array getCode: ", getCode[0][0].TYPE);

    let ibadah = getCode.find((test) => test.TYPE == jenisIbadah).SHORTNAMES;
    // ibadah = ibadah.SHORTNAMES
    console.log("--------------ibadah: ", ibadah);

    let tahun = moment().year().toString().substr(-2);
    console.log("-------------------------------tahun: ", tahun);

    let haiwan = getCode.find((test) => test.TYPE == liveStokType).SHORTNAMES;
    console.log("--------------haiwan: ", haiwan);

    let kawasan = getCode.find(
      (test) => test.TYPE == implementerKawasan
    ).SHORTNAMES;
    console.log("--------------kawasan: ", kawasan);

    let negara = getCode.find(
      (test) => test.TYPE == implementerNegara
    ).SHORTNAMES;
    console.log("--------------negara: ", negara);

    let liveStokBilBahagian = getBilBahagian.find(
      (test) => test.HAIWAN == liveStokType
    ).BAHAGIAN;
    console.log("--------------bahagian: ", liveStokBilBahagian);

    let getConf = await conf.getConfig("haiwan");
    let liveStokTypeCode = getConf.find(
      (test) => test.confLookupCode == liveStokType
    ).confLookupId;
    console.log("--------------liveStokTypeCode: ", liveStokTypeCode);

    for (i = 0; i < stokBaru; i++) {
      latestLiveStokId++;
      liveStokNo = null;
      let idString = Number(latestLiveStokId).toString();
      idString = pad.substring(0, pad.length - idString.length) + idString;
      // console.log("--------------latest livestokid:", idString)

      liveStokNo = ibadah + tahun + "-" + haiwan + kawasan + negara + idString;

      let insertLiveStok = await model.insertLiveStok(
        implementerId,
        implementerLokasiId,
        implementerLokasiHaiwanId,
        liveStokNo,
        liveStokType,
        liveStokTypeCode,
        liveStokBilBahagian,
        jenisIbadah,
        setupKorbanId
      );

      for (j = 1; j <= liveStokBilBahagian; j++) {
        let idString2 = Number(j).toString();
        idString2 =
          pad2.substring(0, pad2.length - idString2.length) + idString2;

        liveStokBahagianNo =
          ibadah +
          tahun +
          "-" +
          haiwan +
          kawasan +
          negara +
          idString +
          "-" +
          idString2;

        console.log("--------------jumlahbahagian: ", jumlahBahagian);
        console.log("--------------idString2: ", idString2);
        console.log("--------------liveStokBahagianNo: ", liveStokBahagianNo);

        let insertLiveStokBahagian = await model.insertLiveStokBahagian(
          latestLiveStokId,
          liveStokBahagianNo,
          setupKorbanId
        );

        if (insertLiveStokBahagian != false) {
          result = {
            status: "berjaya",
            message: "Anda berjaya tambah bahagian stok haiwan.",
          };
        } else {
          result = {
            status: "gagal",
            message: "Anda gagal tambah bahagian stok haiwan.",
          };
        }
      }

      if (insertLiveStok != false) {
        result = {
          status: "berjaya",
          message: "Anda berjaya tambah stok haiwan.",
        };
      } else {
        result = {
          status: "gagal",
          message: "Anda gagal tambah stok haiwan.",
        };
      }
    }

    let updateImplementerLokasiHaiwan =
      await model.updateImplementerLokasiHaiwan(
        implementerLokasiHaiwanId,
        stokBaru,
        jumlahBahagian
      );

    if (updateImplementerLokasiHaiwan != false) {
      result = {
        status: "berjaya",
        message: "Anda berjaya kemaskini stok haiwan.",
      };
    } else {
      result = {
        status: "gagal",
        message: "Anda gagal kemaskini stok haiwan.",
      };
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
