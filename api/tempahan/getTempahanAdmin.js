const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/tempahan/tempahan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");

router.get("/", async (req, res) => {
  let param = null;
  let result = null;

  try {
    // BIND PARAMETER TO VARIABLES
    console.log(req.query);
    param = req.query;
    year = param.Id;
    stat = param.status;

    let getTempahanAdmin = await model.getTempahanAdmin(year, stat);

    if (
      getTempahanAdmin[0] != false ||
      getTempahanAdmin[1] != false ||
      getTempahanAdmin[2] != false
    ) {
      result = {
        status: "berjaya",
        message: "Anda berjaya dapatkan senarai tempahan oleh admin.",
        data: getTempahanAdmin[0],
        data2: getTempahanAdmin[1],
        data3: getTempahanAdmin[2],
      };
    } else {
      result = {
        status: "gagal",
        message: "Anda gagal dapatkan senarai tempahan oleh admin.",
      };
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
