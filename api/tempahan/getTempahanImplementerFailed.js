const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/tempahan/tempahan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
    let param = null;
    let result = null;

    let implementerId = null;

    try {

        // BIND PARAMETER TO VARIABLES
        param = req.query;
        implementerId = param.implementerId;

        let getTempahanImplementerFailed = await model.getTempahanImplementerFailed(implementerId);

        if (getTempahanImplementerFailed[0] != false || getTempahanImplementerFailed[1] != false || getTempahanImplementerFailed[2] != false) {
            result = {
                status: "berjaya",
                message: "Anda berjaya dapatkan senarai tempahan oleh implementer.",
                data: getTempahanImplementerFailed[0],
                data1: getTempahanImplementerFailed[1],
                data2: getTempahanImplementerFailed[2],
            };
        } else {
            result = {
                status: "gagal",
                message: 'Anda gagal dapatkan senarai tempahan oleh implementer.'
            }
        }
    } catch (error) {
        console.log(error); // LOG ERROR
        result = {
            message: `API Error`,
        };
    }

    // RETURN
    res.status(200).json(result);

});

module.exports = router;