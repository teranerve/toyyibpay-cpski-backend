const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/tempahan/tempahan.js"); // INCLUDE FUNCTION FILE

router.get("/", async (req, res) => {
    let param = null;
    let result = null;

    let tempahanDetailsId = null;

    try {

        // BIND PARAMETER TO VARIABLES
        param = req.query;
        tempahanDetailsId = param.tempahanDetailsId;

        let getTempahanTimeline = await model.getTempahanTimeline(tempahanDetailsId);
        if(!getTempahanTimeline){
            result = {
                status: "gagal",
                message: 'Anda gagal dapatkan timeline tempahan.'
            }
        }
        else{
            result = {
                status: "berjaya",
                message: "Anda berjaya dapatkan timeline tempahan.",
                data: getTempahanTimeline,
            };
        }
    } catch (error) {
        console.log(error); // LOG ERROR
        result = {
            message: `API Error`,
        };
    }

    // RETURN
    res.status(200).json(result);

});

module.exports = router;