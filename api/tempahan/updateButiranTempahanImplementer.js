const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/tempahan/tempahan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");

router.post("/", async (req, res) => {
    let param = null;
    let result = null;

    let userId = null;
    let userFullname = null;

    try {
        param = req.body;

        wakilPesertaId = param.wakilPesertaId;
        wakilPesertaName = param.wakilPesertaName;
        console.log(param);

        let updateButiranTempahanImplementer = await model.updateButiranTempahanImplementer(
            wakilPesertaId,
            wakilPesertaName
        )

        if (updateButiranTempahanImplementer != false) {
            result = {
                status: "berjaya",
                message: "Anda berjaya kemaskini Butiran Tempahan.",
            }
        } else {
            result = {
                status: "gagal",
                message: 'Anda gagal kemaskini Butiran Tempahan'
            }
        }

    } catch (error) {
        console.log(error); // LOG ERROR
        result = {
            message: `API Error`,
        };
    }

    // RETURN
    res.status(200).json(result);
})
module.exports = router;