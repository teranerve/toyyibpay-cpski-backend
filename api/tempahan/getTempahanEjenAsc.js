const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/tempahan/tempahan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
    let param = null;
    let result = null;

    let userId = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    userId = param.userId;

      let getTempahanEjenAsc = await model.getTempahanEjenAsc(userId);

      if (getTempahanEjenAsc[0] != false || getTempahanEjenAsc[1] != false || getTempahanEjenAsc[2] != false || getTempahanEjenAsc[3] != false) {
        result = {
          status: "berjaya",
          message: "Anda berjaya dapatkan senarai tempahan oleh ejen.",
          data: getTempahanEjenAsc[0],
          data1: getTempahanEjenAsc[1],
          data2: getTempahanEjenAsc[2],
          data3: getTempahanEjenAsc[3],
        };
      } else {
        result = {
          status: "gagal",
          message: 'Anda gagal dapatkan senarai tempahan oleh ejen.'
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

module.exports = router;