const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/tempahan/tempahan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
    let param = null;
    let result = null;

    let userId = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    userId = param.userId;

      let getTempahanEjen = await model.getTempahanEjen(userId);

      if (getTempahanEjen[0] != false || getTempahanEjen[1] != false || getTempahanEjen[2] != false || getTempahanEjen[3] != false) {
        result = {
          status: "berjaya",
          message: "Anda berjaya dapatkan senarai tempahan oleh ejen.",
          data: getTempahanEjen[0],
          data1: getTempahanEjen[1],
          data2: getTempahanEjen[2],
          data3: getTempahanEjen[3],
        };
      } else {
        result = {
          status: "gagal",
          message: 'Anda gagal dapatkan senarai tempahan oleh ejen.'
        }
      }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

module.exports = router;