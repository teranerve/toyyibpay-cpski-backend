const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/tempahan/tempahan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");

router.get("/", async (req, res) => {
  let param = null;
  let result = null;
  let tempahanId = null;
  try {
    // BIND PARAMETER TO VARIABLES
    param = req.query;
    tempahanId = param.tempahanId;

    let getButiranTempahanAdmin = await model.getButiranTempahanAdmin(
      tempahanId
    );

    if (
      getButiranTempahanAdmin[0] != false ||
      getButiranTempahanAdmin[1] != false ||
      getButiranTempahanAdmin[2] != false
    ) {
      result = {
        status: "berjaya",
        message: "Anda berjaya dapatkan butiran tempahan oleh admin.",
        data: getButiranTempahanAdmin[0],
        data1: getButiranTempahanAdmin[1],
        data2: getButiranTempahanAdmin[2],
        data3: getButiranTempahanAdmin[3],
        data4: getButiranTempahanAdmin[4],
      };
    } else {
      result = {
        status: "gagal",
        message: "Anda gagal dapatkan butiran tempahan oleh admin.",
      };
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

router.get("/getCallback", async (req, res) => {
  let param = null;
  let result = null;
  let modalNoTempahan = null;
  try {
    // console.log(req.query)

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    modalNoTempahan = param.modalNoTempahan;
    if (modalNoTempahan == null || modalNoTempahan == "") {
      result = {
        status: "Gagal",
        message: "modalNoTempahan tidak boleh kosong atau tidak wujud",
      };
    }

    let getCallback = await model.getCallback(modalNoTempahan);
    if (!getCallback) {
      result = {
        status: "Gagal",
        message: "Anda gagal dapatkan callback tempahan oleh admin.",
      };
      return;
    } else {
      console.log("getCallback");
      console.log(getCallback);
      result = {
        status: "Berjaya",
        message: "Anda berjaya dapatkan callback tempahan oleh admin.",
        data: getCallback,
      };
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

router.get("/getRefund", async (req, res) => {
  let param = null;
  let result = null;
  let tempahanId = null;
  try {
    // console.log(req.query)

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    tempahanId = param.tempahanId;
    if (tempahanId == null || tempahanId == "") {
      result = {
        status: "Gagal",
        message: "tempahanId tidak boleh kosong atau tidak wujud",
      };
    }

    let getRefund = await model.getRefund(tempahanId);
    if (!getRefund) {
      result = {
        status: "Gagal",
        message: "Anda gagal dapatkan refund tempahan oleh admin.",
      };
      return;
    } else {
      console.log("getRefund");
      console.log(getRefund);
      result = {
        status: "Berjaya",
        message: "Anda berjaya dapatkan refund tempahan oleh admin.",
        data: getRefund,
      };
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});
module.exports = router;
