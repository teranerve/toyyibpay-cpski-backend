const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/tempahan/tempahan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
    let param = null;
    let result = null;
    let tempahanId = null;
    try {

        // BIND PARAMETER TO VARIABLES
        param = req.query;
        tempahanId = param.tempahanId;


        let getButiranTempahanEjen = await model.getButiranTempahanEjen(tempahanId);

        if (getButiranTempahanEjen[0] != false || getButiranTempahanEjen[1] != false || getButiranTempahanEjen[2] != false) {
            result = {
                status: "berjaya",
                message: "Anda berjaya dapatkan butiran tempahan oleh admin.",
                data: getButiranTempahanEjen[0],
                data1: getButiranTempahanEjen[1],
                data2: getButiranTempahanEjen[2],
                data3: getButiranTempahanEjen[3]
            };
        } else {
            result = {
                status: "gagal",
                message: 'Anda gagal dapatkan butiran tempahan oleh admin.'
            }
        }
    } catch (error) {
        console.log(error); // LOG ERROR
        result = {
            message: `API Error`,
        };
    }

    // RETURN
    res.status(200).json(result);

});

module.exports = router;