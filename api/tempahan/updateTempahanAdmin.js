const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/tempahan/tempahan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");

router.post("/", async (req, res) => {
    let param = null;
    let result = null;

    let tempahanId = null;
    let resitPaymentMethod = null;
    let resitRefNo = null;
    let resitPaymentStatus = null;
    let tempahanTarikhBayaran = null;
    let tempahanStatus = null;
    let tempahanTarikhUploadResit = null;

    try {
        param = req.body;

        tempahanId = param.tempahanId;
        resitPaymentMethod = param.resitPaymentMethod;
        resitRefNo = param.resitRefNo;
        resitPaymentStatus = param.resitPaymentStatus;
        tempahanTarikhBayaran = param.tempahanTarikhBayaran;
        tempahanStatus = param.tempahanStatus;
        tempahanTarikhUploadResit = param.tempahanTarikhUploadResit;
        console.log(param);

        let updateTempahanAdmin = await model.updateTempahanAdmin(
            tempahanId,
            tempahanTarikhBayaran,
            tempahanStatus,
            tempahanTarikhUploadResit
        )

        if (updateTempahanAdmin != false) {
            result = {
                status: "berjaya",
                message: "Anda berjaya kemaskini Tempahan.",
            }
        } else {
            result = {
                status: "gagal",
                message: 'Anda gagal kemaskini Tempahan'
            }
        }

        let updateResitAdmin = await model.insertResitAdmin(
            tempahanId,
            resitPaymentMethod,
            resitRefNo,
            resitPaymentStatus
        )

        if (updateResitAdmin != false) {
            result = {
                status: "berjaya",
                message: "Anda berjaya kemaskini Resit.",
            }
        } else {
            result = {
                status: "gagal",
                message: 'Anda gagal kemaskini Resit'
            }
        }

    } catch (error) {
        console.log(error); // LOG ERROR
        result = {
            message: `API Error`,
        };
    }

    // RETURN
    res.status(200).json(result);
})
module.exports = router;