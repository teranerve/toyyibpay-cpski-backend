const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/tempahan/tempahan.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
    let param = null;
    let result = null;
    let status = null;
    try {

        // BIND PARAMETER TO VARIABLES
        param = req.query;
        status = param.status;


        let getTempahanAdminByStatus = await model.getTempahanAdminByStatus(status);

        if (getTempahanAdminByStatus != false) {
            result = {
                status: "berjaya",
                message: "Anda berjaya dapatkan senarai tempahan oleh admin mengikut status.",
                data: getTempahanAdminByStatus,
            };
        } else {
            result = {
                status: "gagal",
                message: 'Anda gagal dapatkan senarai tempahan oleh admin mengikut status.'
            }
        }
    } catch (error) {
        console.log(error); // LOG ERROR
        result = {
            message: `API Error`,
        };
    }

    // RETURN
    res.status(200).json(result);

});

module.exports = router;