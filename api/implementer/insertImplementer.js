const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/implementer/implementer.js"); // INCLUDE FUNCTION FILE

// MAKE SURE METHOD IS CORRECT WHEN CALLING API

// POST USER
router.post("/", async (req, res) => {
  let param = null;
  let result = null;

  // VARIABLE SETUP
  let username = null;
  let password = null;
  let fullname = null;
  let email = null;
  let phoneno = null;
  let kawasan = null;
  let negara = null;
  let ibadah = null;
  let alamat = null;
  let postcode = null;
  let bandar = null;
  let negeri = null;
  let noakaun = null;
  let namapemegangbank = null;
  let namabank = null;
  let bankcode = null;
  let closedate = null;
  let jenis = null;
  let hargaekor = null;
  let hargabhg = null;
  let statusHaiwanKorban = null;
  //   let haiwan = null;

  try {
    // BIND PARAMETER TO VARIABLES
    param = req.body;

    username = param.username;
    password = param.password;
    fullname = param.fullname;
    email = param.email;
    phoneno = param.phoneno;
    kawasan = param.kawasan;
    negara = param.negara;
    ibadah = param.ibadah;
    alamat = param.alamat;
    postcode = param.postcode;
    bandar = param.bandar;
    negeri = param.negeri;
    noakaun = param.noakaun;
    namapemegangbank = param.namapemegangbank;
    namabank = param.namabank;
    bankcode = param.bankcode;
    closedate = param.closedate;
    jenis = param.jenis;
    hargaekor = param.hargaekor;
    hargabhg = param.hargabhg;
    statusHaiwanKorban = param.statusHaiwanKorban;

    let setupKorbanId = await model.getCurrentSetupKorbanId();

    if (!setupKorbanId) throw "SetupKorbanId Not Found!";

    //VALIDATE USERNAME & EMAIL
    let checkUsernameEmail = await model.checkUsernameEmail(
      username,
      email,
      phoneno
    );

    if (checkUsernameEmail == false) {
      result = {
        status: "gagal",
        message: "Username, Email atau Nombor Telefon telah wujud.",
      };
    } else {
      // GET USER FUNCTION
      let insertImplementer = await model.insertImplementer(
        username,
        fullname,
        email,
        password,
        phoneno,
        negeri,
        negara,
        ibadah,
        setupKorbanId
      );

      let insertImplementerLokasi = await model.insertImplementerLokasi(
        insertImplementer,
        alamat,
        postcode,
        bandar,
        negeri,
        noakaun,
        namapemegangbank,
        namabank,
        bankcode,
        closedate,
        setupKorbanId
      );

      let insertImplementerLokasiHaiwan =
        await model.insertImplementerLokasiHaiwan(
          insertImplementer,
          insertImplementerLokasi,
          jenis,
          hargaekor,
          hargabhg,
          statusHaiwanKorban,
          setupKorbanId
        );

      let insertsetupKorbanImplementerLokasi =
        await model.insertsetupKorbanImplementerLokasi(
          insertImplementer,
          insertImplementerLokasi,
          setupKorbanId
        );

      if (insertImplementer != false) {
        result = {
          message: "anda berjaya masukkan Implementer",
          environment: process.env.ENVIRONMENT,
          userdata: insertImplementer,
          test: insertImplementer,
        };
      } else {
        result = {
          message: "User Insert fail",
        };
      }
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
