const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../function/user.js"); // INCLUDE FUNCTION FILE

// MAKE SURE METHOD IS CORRECT WHEN CALLING API

// GET USER
router.get("/",  async (req, res) => {
  let param = null;
  let result = null;

  // VARIABLE SETUP
  let userType = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.query;
    userType = param.userType;

    // GET USER FUNCTION
    const getUser = await model.getUser(userType);

    if (getUser != false) {
      result = {
        message: "anda berjaya dapatkan senarai pengguna",
        environment: process.env.ENVIRONMENT,
        userdata: getUser,
        test: getUser,
      };
    } else {
      result = {
          message: 'User Type does not exist'
      }
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
