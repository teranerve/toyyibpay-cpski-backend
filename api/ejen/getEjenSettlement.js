const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/ejen/ejen.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/",  async (req, res) => {
    let param = null;
    let result = null;

    try {

        // BIND PARAMETER TO VARIABLES
        param = req.query;
            
        // GET USER FUNCTION
        let getEjenSettlement = await model.getEjenSettlement();
        if(!getEjenSettlement) {
            result = {
                status: "gagal",
                message: 'Anda gagal dapatkan senarai ejen',
                data: null,
            }
        }
        else{
            // console.log('getEjenSettlement')
            // console.log(getEjenSettlement)
            result = {
            status: "berjaya",
            message: "Anda berjaya dapatkan senarai settlement ejen",
            environment: process.env.ENVIRONMENT,
            data: getEjenSettlement,
          };
        }
      } catch (error) {
        console.log(error); // LOG ERROR
        result = {
          message: `API Error`,
        };
      }
    
      // RETURN
      res.status(200).json(result);
});

router.get("/settlementDetails",  async (req, res) => {
    let param = null;
    let result = null;
    let settlementLookupId = null;

    try {
        // BIND PARAMETER TO VARIABLES
        param = req.query;
        settlementLookupId = param.settlementLookupId;

        if(settlementLookupId == '' || settlementLookupId == null){
            result = {
                status: "gagal",
                message: 'settlementLookupId tidak boleh kosong',
                data: null,
            }
        }
        else {
            // GET EJEN SETTLEMENT DETAIL FUNCTION
            let getEjenSettlementDetails = await model.getEjenSettlementDetails(settlementLookupId);
            if(!getEjenSettlementDetails) {
                result = {
                    status: "gagal",
                    message: 'Anda gagal dapatkan senarai ejen',
                    data: null,
                }
            }
            else {
                // console.log('getEjenSettlement')
                // console.log(getEjenSettlement)
                result = {
                    status: "berjaya",
                    message: "Anda berjaya dapatkan senarai settlement ejen",
                    environment: process.env.ENVIRONMENT,
                    data: getEjenSettlementDetails[0],
                    data2: getEjenSettlementDetails[1],
                }
            }
        }        
    } 
    catch (error) {
        console.log(error); // LOG ERROR
        result = {
            message: `API Error`,
        };
    }
    

    // RETURN
    res.status(200).json(result);
});

router.post("/settlementDate",  async (req, res) => {
    let param = null;
    let result = null;
    let settlementLookupDate = null;

    try {
        // BIND PARAMETER TO VARIABLES
        param = req.body;
        settlementLookupDate = param.settlementLookupDate;

        if(settlementLookupDate == '' || settlementLookupDate == null){
            result = {
                status: "gagal",
                message: 'Tarikh Settlement Baru tidak boleh kosong',
                data: null,
            }
        }
        else {
            // INSERT NEW SETTLEMENT INTO SETTLEMENTLOOKUP TABLE
            let insertSettlementDate = await model.insertSettlementDate(settlementLookupDate);
            if(!insertSettlementDate) {
                result = {
                    status: "gagal",
                    message: 'Anda gagal masukkan tarikh baharu untuk settlement',
                    data: null,
                }
            }
            else {
                result = {
                    status: "berjaya",
                    message: "Anda berjaya masukkan tarikh baharu untuk settlement",
                    environment: process.env.ENVIRONMENT,
                    data: insertSettlementDate,
                }
            }
        }        
    } 
    catch (error) {
        console.log(error); // LOG ERROR
        result = {
            message: `API Error`,
        };
    }
    

    // RETURN
    res.status(200).json(result);
});

router.get("/settlementTempahan",  async (req, res) => {
    let param = null;
    let result = null;
    let settlementLookupId = null;

    try {
        // BIND PARAMETER TO VARIABLES
        param = req.query;
        settlementLookupId = param.settlementLookupId;
        userEjenId = param.userEjenId;

        if(settlementLookupId == '' || settlementLookupId == null){
            result = {
                status: "gagal",
                message: 'settlementLookupId tidak boleh kosong',
                data: null,
            }
        }
        else if(userEjenId == '' || userEjenId == null){
            result = {
                status: "gagal",
                message: 'userEjenId tidak boleh kosong',
                data: null,
            }
        }
        else {
            // GET EJEN SETTLEMENT DETAIL FUNCTION
            let getEjenSettlementTempahan = await model.getEjenSettlementTempahan(settlementLookupId,userEjenId);
            if(!getEjenSettlementTempahan) {
                result = {
                    status: "gagal",
                    message: 'Anda gagal dapatkan senarai ejen',
                    data: null,
                }
            }
            else {
                // console.log('getEjenSettlement')
                // console.log(getEjenSettlement)
                result = {
                    status: "berjaya",
                    message: "Anda berjaya dapatkan senarai settlement ejen",
                    environment: process.env.ENVIRONMENT,
                    data: getEjenSettlementTempahan
                }
            }
        }        
    } 
    catch (error) {
        console.log(error); // LOG ERROR
        result = {
            message: `API Error`,
        };
    }
    

    // RETURN
    res.status(200).json(result);
});

router.post("/updateEjenCommission",  async (req, res) => {
    let param = null;
    let result = null;
    let settlementLookupId = null;
    let userEjenId = null;
    let transactionNo = null;

    try {
        // BIND PARAMETER TO VARIABLES
        param = req.body;
        console.log('param' + JSON.stringify(param))
        settlementLookupId = param.settlementLookupId;
        userEjenId = param.userEjenId;
        transactionNo = param.transactionNo;

        if(settlementLookupId == '' || settlementLookupId == null){
            result = {
                status: "gagal",
                message: 'settlementLookupId tidak boleh kosong',
                data: null,
            }
        }
        else if(userEjenId == '' || userEjenId == null){
            // UPDATE SETTLEMENT FOR ALL EJEN UNDER SPECIFIC SETTLEMENT DATE
            let updateAllEjenCommission = await model.updateAllEjenCommission(settlementLookupId,transactionNo);
            if(!updateAllEjenCommission) {

                let countActiveSettlement = await model.countActiveSettlement(settlementLookupId,transactionNo);
                if(!countActiveSettlement) {
                    result = {
                        status: "gagal",
                        message: 'Anda gagal kemaskini komisyen untuk keseluruhan ejen',
                        data: null,
                    }
                }
                else {
                    // console.log('countActiveSettlement')
                    // console.log(countActiveSettlement.count)
                    if(countActiveSettlement.count == 0){
                        result = {
                            status: "gagal",
                            message: 'Keseluruhan settlement telah berjaya dikemaskini',
                            data: null,
                        }
                    }
                    else {
                        result = {
                            status: "gagal",
                            message: 'Anda gagal kemaskini komisyen untuk keseluruhan ejen',
                            data: null,
                        }
                    }
                    
                }
                
            }
            else {
                result = {
                    status: "berjaya",
                    message: "Anda berjaya kemaskini komisyen untuk keseluruhan ejen",
                    environment: process.env.ENVIRONMENT,
                    data: updateAllEjenCommission,
                }
            }
        }
        else if(transactionNo == '' || transactionNo == null){
            result = {
                status: "gagal",
                message: 'transactionNo tidak boleh kosong',
                data: null,
            }
        }
        else {
            // INSERT NEW SETTLEMENT INTO SETTLEMENTLOOKUP TABLE
            let updateEjenCommission = await model.updateEjenCommission(settlementLookupId,userEjenId,transactionNo);
            if(!updateEjenCommission) {
                result = {
                    status: "gagal",
                    message: 'Anda gagal kemaskini komisyen untuk ejen',
                    data: null,
                }
            }
            else {
                result = {
                    status: "berjaya",
                    message: "Anda berjaya kemaskini komisyen untuk ejen",
                    environment: process.env.ENVIRONMENT,
                    data: updateEjenCommission,
                }
            }
        }        
    } 
    catch (error) {
        console.log(error); // LOG ERROR
        result = {
            message: `API Error`,
        };
    }
    

    // RETURN
    res.status(200).json(result);
});

module.exports = router;