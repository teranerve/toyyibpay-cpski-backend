const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/ejen/ejen.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/",  async (req, res) => {
    let param = null;
    let result = null;

    let userId = null;

    try {

        // BIND PARAMETER TO VARIABLES
        param = req.query;
        userId = param.userId;
    
        // GET USER FUNCTION
        let getButiranEjen = await model.getButiranEjen(userId);
    
        if (getButiranEjen != false) {
          result = {
            status: "berjaya",
            message: "Anda berjaya dapatkan butiran ejen",
            data: getButiranEjen,
          };
        } else {
          result = {
            status: "gagal",
            message: 'Anda gagal dapatkan butiran ejen'
          }
        }
      } catch (error) {
        console.log(error); // LOG ERROR
        result = {
          message: `API Error`,
        };
      }
    
      // RETURN
      res.status(200).json(result);

});

module.exports = router;