const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/ejen/ejen.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");

router.get("/", async (req, res) => {
  let param = null;
  let result = null;

  try {
    // BIND PARAMETER TO VARIABLES
    param = req.query;

    let ejenDuta = await model.getEjenDuta(param.ejenDuta);

    // GET USER FUNCTION
    let ejenCawangan = await model.getEjenCawangan(param.ejenDuta, param.tahun);
    // console.log("ejenCawangan: ", ejenCawangan);
    if (!ejenCawangan) {
      result = {
        status: "gagal",
        message: "Anda gagal dapatkan senarai ejen cawangan",
        data: null,
      };
    } else {
      // console.log('getEjenSettlement')
      // console.log(getEjenSettlement)

      // Sum of all transaction (jumlah transaksi)
      let totaltransaction = null;
      let totalJualan = null;
      for (let i = 0; i < ejenCawangan.length; i++) {
        totaltransaction += ejenCawangan[i].tempahanCount;
        totalJualan += ejenCawangan[i].tempahanTotalPrice;
      }
      result = {
        status: "berjaya",
        message: "Anda berjaya dapatkan senarai settlement ejen cawangan",
        environment: process.env.ENVIRONMENT,
        duta: ejenDuta.userUsername,
        jumlahTransaksi: totaltransaction,
        jumlahJualan: totalJualan,
        data: ejenCawangan,
      };
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
