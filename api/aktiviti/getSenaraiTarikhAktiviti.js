const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/aktiviti/aktiviti.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");

router.get("/", async (req, res) => {
  let param = null;
  let result = null;
  let setupKorbanAktivitiId = null;

  console.log(req.query);

  try {
    // BIND PARAMETER TO VARIABLES
    param = req.query;
    setupKorbanAktivitiId = param.setupKorbanAktivitiId;

    let getTarikhAktiviti = await model.getTarikhAktiviti(
      setupKorbanAktivitiId
    );

    console.log("getTarikhAktiviti");
    console.log(getTarikhAktiviti[0]);

    if (getTarikhAktiviti != false) {
      result = {
        status: "berjaya",
        message: "Anda berjaya dapatkan senarai tarikh aktiviti",
        data: getTarikhAktiviti[0],
        data2: getTarikhAktiviti[1],
        implementerId: getTarikhAktiviti[2],
      };
    } else {
      result = {
        status: "gagal",
        message: "Anda gagal dapatkan senarai tarikh aktiviti",
        data: null,
        data2: null,
      };
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

router.get("/implementerDetails", async (req, res) => {
  let param = null;
  let result = null;
  let aktivitiDateId = null;

  console.log(req.query);

  try {
    // BIND PARAMETER TO VARIABLES
    param = req.query;
    aktivitiDateId = param.aktivitiDateId;

    let getImplementerAktivitiDate = await model.getImplementerAktivitiDate(
      aktivitiDateId
    );

    console.log("getImplementerAktivitiDate");
    console.log(getImplementerAktivitiDate);

    if (getImplementerAktivitiDate != false) {
      result = {
        status: "berjaya",
        message: "Anda berjaya dapatkan butiran implementer",
        data: getImplementerAktivitiDate[0],
      };
    } else {
      result = {
        status: "gagal",
        message: "Anda gagal dapatkan butiran implementer",
        data: null,
      };
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});
module.exports = router;
