const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/haiwanKorban/haiwanKorban.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");

router.get("/", async (req, res) => {
  let param = null;
  let result = null;
  let jenisHaiwan = null;
  let jenisIbadah = null;

  try {
    // BIND PARAMETER TO VARIABLES
    param = req.query;
    jenisHaiwan = param.jenisHaiwan;
    jenisIbadah = param.jenisIbadah;
    year = param.Id;

    // console.log("PARAM", param);
    let getButiranHaiwanKorban = await model.getButiranHaiwanKorban(
      jenisHaiwan,
      jenisIbadah,
      year
    );

    if (
      getButiranHaiwanKorban[0] != false &&
      getButiranHaiwanKorban[1] != false
    ) {
      result = {
        status: "berjaya",
        message: "Anda berjaya dapatkan senarai butiran haiwan korban.",
        jumlahHaiwanKorban: getButiranHaiwanKorban[0],
        jumlahBaki: getButiranHaiwanKorban[1],
      };
    } else {
      result = {
        status: "gagal",
        message: "Anda gagal dapatkan senarai butiran haiwan korban.",
      };
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
