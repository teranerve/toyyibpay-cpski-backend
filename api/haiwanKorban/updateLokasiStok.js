const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/haiwanKorban/updateLokasiStok.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");

router.post("/", async (req, res) => {
  try {
    const param = model.check_param(req.body);
    if (!param.status)
      return res.status(400).json({ status: "gagal", message: param.message });

    // console.log(req.body);

    // return res.status(200).json({
    //   status: "berjaya",
    //   message: "Berjaya menukar lokasi stok",
    //   // data: result,
    // });

    const result = await model.update_stok_location(req.body);
    if (!result)
      return res.status(400).json({
        status: "gagal",
        message: "Senarai stok tidak sah. Proses terhenti.",
      });

    return res.status(200).json({
      status: "berjaya",
      message: "Berjaya menukar lokasi stok",
      data: result,
    });
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }
});

module.exports = router;
