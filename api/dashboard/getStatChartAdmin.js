const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/dashboard/dashboard.js"); // INCLUDE FUNCTION FILE
const moment = require("moment");


router.get("/", async (req, res) => {
  let result = null;
  let param = req.query;
  let year = param.Id;

  try {
    let getStatChartAdmin = await model.getStatChartAdmin(year);

    if (getStatChartAdmin[0] != false && getStatChartAdmin[1] != false && getStatChartAdmin[2] != false && getStatChartAdmin[3] != false && getStatChartAdmin[4] != false) {
      result = {
        status: "berjaya",
        message: "Anda berjaya dapatkan data Stat Chart",
        environment: process.env.ENVIRONMENT,
        statChartData1: getStatChartAdmin[0][0],
        statChartData1_1: getStatChartAdmin[1][0],
        statChartData2: getStatChartAdmin[2][0],
        statChartData2_1: getStatChartAdmin[3][0],
        statChartData3: getStatChartAdmin[4][0],
        statChartData3_1: getStatChartAdmin[5][0],
        statChartData4: getStatChartAdmin[6][0],
        // statChartData4_1: getStatChartAdmin[7][0]
      };
    } else {
      result = {
        status: "gagal",
        message: 'Anda gagal dapatkan data Stat Chart'
      }
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);

});

module.exports = router;