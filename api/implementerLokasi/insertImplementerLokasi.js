const express = require("express"); // MUST HAVE
const router = express.Router(); // MUST HAVE
const model = require("../../function/implementerLokasi/implementerLokasi.js"); // INCLUDE FUNCTION FILE

// MAKE SURE METHOD IS CORRECT WHEN CALLING API

// POST USER
router.post("/",  async (req, res) => {
  let param = null;
  let result = null;

  // VARIABLE SETUP
  let alamat = null;
  let postcode = null;
  let bandar = null;
  let negeri = null;
  let noakaun = null;
  let namapemegangbank = null;
  let namabank = null;
  let bankcode = null;

  try {

    // BIND PARAMETER TO VARIABLES
    param = req.body;
    alamat = param.alamat;
    postcode = param.postcode;
    bandar = param.bandar;
    negeri = param.negeri;
    noakaun = param.noakaun;
    namapemegangbank = param.namapemegangbank;
    namabank = param.namabank;
    bankcode = param.bankcode;

    // GET USER FUNCTION
    const insertImplementerLokasi = await model.insertImplementerLokasi(alamat,postcode,bandar,negeri,noakaun,namapemegangbank,namabank,bankcode);

    if (insertImplementerLokasi != false) {
      result = {
        message: "anda berjaya masukkan Implementer",
        environment: process.env.ENVIRONMENT,
        userdata: insertImplementerLokasi,
        test: insertImplementerLokasi,
      };
    } else {
      result = {
          message: 'User Insert fail'
      }
    }
  } catch (error) {
    console.log(error); // LOG ERROR
    result = {
      message: `API Error`,
    };
  }

  // RETURN
  res.status(200).json(result);
});

module.exports = router;
